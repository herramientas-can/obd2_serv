/*
    Copyright 2021 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
   
    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */
/*
 * Control de Cambios
 *
 * 22/10/2021   mapv    Escrito
 * 25/10/2021   mapv    Se añade comando para solicitar todos los PIDs disponibles en un servicio
 * 27/12/2021   mapv    Se añaden ordenes para la gestión de buffer.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "can.h"
#include "obd2.h"
#include "ordenes.h"
#include "lenguaje.h"
#include "buffer.h"

int syn_setecu( int p )
{
	/*
	unsigned int serv, lon;
	*/

	int addr;

	if( can_get_verbose() > 0 )
		printf("setecu(%d)\n", p );

	addr = get_ecu_address();

	if( addr == p )
		return 0;

	set_ecu_address( p );

	/*
	serv = obd2_get_serv();
	lon = obd2_get_long();

	obd2_set_serv( 0x3E );
	obd2_set_long( 0x2 );

	obd2_send(1);

	obd2_set_serv( serv );
	obd2_set_long( lon );
	*/

	return 0;
}

int syn_setlen( int p )
{
	if( can_get_verbose() > 0 )
		printf("setlen(%d)\n", p );

	obd2_set_long( (unsigned int) p );

	return 0;
}

int syn_setser( int p )
{
	if( can_get_verbose() > 0 )
		printf("setser(%d)\n", p );

	obd2_set_serv( (unsigned int) p );

	return 0;
}

int syn_setpid( int p )
{
	if( can_get_verbose() > 0 )
		printf("setpid(%d)\n", p );

	obd2_set_pid( (unsigned int) p );

	return 0;
}

int syn_settmo( int p )
{
	if( can_get_verbose() > 0 )
		printf("settmo(%d)\n", p );

	can_set_timeout( p );

	return 0;
}

int syn_settrc( int p )
{
	if( can_get_verbose() > 0 )
		printf("settrc(%d)\n", p );

	can_set_verbose( p );

	return 0;
}

int syn_setd0( int p )
{
	if( can_get_verbose() > 0 )
		printf("setd0;(%d)\n", p );

	obd2_set_dato( 0, (unsigned int) p );

	return 0;
}

int syn_setd1( int p )
{
	if( can_get_verbose() > 0 )
		printf("setd1(%d)\n", p );

	obd2_set_dato( 1, (unsigned int) p );

	return 0;
}

int syn_setd2( int p )
{
	if( can_get_verbose() > 0 )
		printf("setd2(%d)\n", p );

	obd2_set_dato( 2, (unsigned int) p );

	return 0;
}

int syn_setd3( int p )
{
	if( can_get_verbose() > 0 )
		printf("setd3(%d)\n", p );

	obd2_set_dato( 3, (unsigned int) p );

	return 0;
}

int syn_setd4( int p )
{
	if( can_get_verbose() > 0 )
		printf("setd4(%d)\n", p );

	obd2_set_dato( 4, (unsigned int) p );

	return 0;
}

int syn_setd5( int p )
{
	if( can_get_verbose() > 0 )
		printf("setd5(%d)\n", p );

	obd2_set_dato( 5, (unsigned int) p );

	return 0;
}

int syn_setd6( int p )
{
	if( can_get_verbose() > 0 )
		printf("setd6(%d)\n", p );

	obd2_set_dato( 6, (unsigned int) p );

	return 0;
}

int syn_setd7( int p )
{
	if( can_get_verbose() > 0 )
		printf("setd7(%d)\n", p );

	obd2_set_dato( 7, (unsigned int) p );

	return 0;
}

int syn_setext( int p )
{
	if( can_get_verbose() > 0 )
		printf("setext(%d)\n", p );

	if( p >= 0x40 && p <= 0xFF )
		set_ext_address( p );
	else
		printf("La dirección extendida tiene que estar comprendida entre 0x40 y 0xFF\n");

	return 0;
}

int syn_send( int p )
{
	int rc;

	if( can_get_verbose() > 0 )
		printf("send(%d)\n", p );

	rc = obd2_send(p);

	return rc;
}

int syn_help( int p )
{
	int i;

	if( can_get_verbose() > 0 )
		printf("help(%d)\n", p );

	for( i = 0; i < MAX_ORDERS; i++ )
		if( strcmp( obd2_syntax[i].orden, ""  ) )
			printf("%s\t- %s\n", obd2_syntax[i].orden, obd2_syntax[i].desc );

	return 0;
}

int syn_exit( int p )
{
	if( can_get_verbose() > 0 )
		printf("exit(%d)\n", p );

	exit(p);

	/* NOTREACHED */
}

int syn_showecu( int p )
{
	if( can_get_verbose() > 0 )
		printf("showecu(%d)\n", p );

	printf("ECU (0x%X)\n", get_ecu_address() );

	return 0;
}

int syn_showlen( int p )
{
	if( can_get_verbose() > 0 )
		printf("showlen(%d)\n", p );

	printf("Longitud: %d\n", obd2_get_long() );

	return 0;
}

int syn_showser( int p )
{
	if( can_get_verbose() > 0 )
		printf("showser(%d)\n", p );

	printf("Servicio: 0x%02X\n", obd2_get_serv() );

	return 0;
}

int syn_showpid( int p )
{
	if( can_get_verbose() > 0 )
		printf("showpid(%d)\n", p );

	printf("PID ...: 0x%02X\n", obd2_get_pid() );

	return 0;
}

int syn_showtmo( int p )
{
	if( can_get_verbose() > 0 )
		printf("showtmo(%d)\n", p );

	printf("Timeout: %d s.\n", can_get_timeout() );

	return 0;
}

int syn_showtrc( int p )
{
	if( can_get_verbose() > 0 )
		printf("showtrc(%d)\n", p );

	printf("Nivel de traza: %d\n", can_get_verbose() );

	return 0;
}

int syn_showd0( int p )
{
	if( can_get_verbose() > 0 )
		printf("showd0;(%d)\n", p );

	printf("D0: %s\n", obd2_get_dato(0) );

	return 0;
}

int syn_showd1( int p )
{
	if( can_get_verbose() > 0 )
		printf("showd1(%d)\n", p );

	printf("D1: %s\n", obd2_get_dato(1) );

	return 0;
}

int syn_showd2( int p )
{
	if( can_get_verbose() > 0 )
		printf("showd2(%d)\n", p );

	printf("D2: %s\n", obd2_get_dato(2) );

	return 0;
}

int syn_showd3( int p )
{
	if( can_get_verbose() > 0 )
		printf("showd3(%d)\n", p );

	printf("D3: %s\n", obd2_get_dato(3) );

	return 0;
}

int syn_showd4( int p )
{
	if( can_get_verbose() > 0 )
		printf("showd4(%d)\n", p );

	printf("D4: %s\n", obd2_get_dato(4) );

	return 0;
}

int syn_showd5( int p )
{
	if( can_get_verbose() > 0 )
		printf("showd5(%d)\n", p );

	printf("D5: %s\n", obd2_get_dato(5) );

	return 0;
}

int syn_showd6( int p )
{
	if( can_get_verbose() > 0 )
		printf("showd6(%d)\n", p );

	printf("D6: %s\n", obd2_get_dato(6) );

	return 0;
}

int syn_showd7( int p )
{
	if( can_get_verbose() > 0 )
		printf("showd7(%d)\n", p );

	printf("D7: %s\n", obd2_get_dato(7) );

	return 0;
}

int syn_showext( int p )
{
	int ext;

	if( can_get_verbose() > 0 )
		printf("showext(%d)\n", p );

	ext = get_ext_address();

	if( ext >= 0x40 )
		printf("Dirección extendida: 0x%2X\n", get_ext_address() );
	else
		printf("Dirección extendida: No definida\n");

	return 0;
}

int syn_showdtc( int p )
{
	if( can_get_verbose() > 0 )
		printf("showdtc(%d)\n", p );

	return 0;
}

int syn_chkpid( int p )
{
	if( can_get_verbose() > 0 )
		printf("chkpid(%d)\n", p );

	return 0;
}

int syn_execute( int p )
{
	int rc;
	char * str;

	str = get_strparam();

	if( can_get_verbose() > 0 )
		printf("execute(%d) - %s\n", p, str );

	rc = interprete( str );

	return rc;
}


int syn_status( int p )
{
	int aux;

	if( can_get_verbose() > 0 )
		printf("status(%d)\n", p );

	aux = get_ecu_address();

	printf("\nECU ......: 0x%03X (%04d).\t", aux, aux );

	aux = get_ext_address();

	if( aux >= 0x40 )
                printf("Dirección extendida: 0x%02X (%03d)\n", aux, aux );
        else
                printf("Dirección extendida: No definida\n");

	aux = obd2_get_long();

	printf("\nLongitud .: 0x%02X (%03d).  \t", aux, aux );

	aux = obd2_get_serv();

	printf("Servicio: 0x%02X (%03d).\t\t", aux, aux );

	aux = obd2_get_pid();

	printf("PID ....: 0x%02X (%03d).\n", aux, aux );

	printf("\nDATOS:\n");

	for(aux = 0; aux < 8; aux++ )
		printf("D%d .......: %s\n", aux, obd2_get_dato(aux) );

	printf("\nTimeout : %d.\tNivel de traza: %d\n", can_get_timeout(), can_get_verbose() );

	return 0;
}

int syn_scanser( int s )
{
	unsigned int p, l;

	int rc;

	if( can_get_verbose() > 0 )
		printf("scanser(%d)\n", s );

	obd2_set_serv( (unsigned int) s );

	p = obd2_get_pid();
	l = obd2_get_long();

	obd2_set_pid( 0x00 );
	obd2_set_long( 0x2 );

	obd2_send(1);

	obd2_set_pid( p );
	obd2_set_long( l );

	rc = obd2_scan( s );

	return rc;
}


int syn_setbuffer( int p )
{
	int len;
	char * str, *ptr;

	if( can_get_verbose() > 0 )
		printf("syn_setbuffer(%d)\n", p );

	str = get_strparam();

	ptr = str;

	while( *ptr != '\0' )
	{
		if( !isalnum( *ptr ) )
		{
			return -1;
		}

		if( *ptr >= 'a' )
			*ptr = *ptr - ('a' - 'A');
		ptr++;
	}

	len = strlen( str );

	if( len % 2 )
		len++;

	set_buffer_len( len / 2 );

	set_hexbuffer( str );

	return 0;
}


int syn_showbuffer( int s )
{
	if( can_get_verbose() > 0 )
		printf("syn_showbuffer(%d)\n", s );

	printf("HEX: 0x%s\n", get_hexbuffer() );

	return 0;
}

int syn_sendbuffer( int s )
{
	int rc; 
	unsigned int l;

	if( can_get_verbose() > 0 )
		printf("syn_sendbuffer(%d)\n", s );

	l = (unsigned int) get_buffer_len();

	if( l > 0 )
		rc = obd2_send_buffer( l, get_buffer(), s );

	return rc;
}

int syn_showlenbuffer( int s )
{
	unsigned int l;

	if( can_get_verbose() > 0 )
		printf("syn_showlenbuffer(%d)\n", s );

	l = (unsigned int) get_buffer_len();

	printf("Longitud del buffer: %d (0x%02X)\n", l, l );

	return 0;
}

