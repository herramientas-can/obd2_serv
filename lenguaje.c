/*
    Copyright 2021 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
   
    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */

/*
 * Control de Cambios
 *
 * 22/10/2021   mapv    Escrito
 * 26/12/2021   mapv    Se añaden las funciones de gestión de buffer.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "ordenes.h"

#define STR_LEN		(192)

char * ofilename = "/tmp/salida.temp.txt";

char strparam[STR_LEN/2];

char * get_strparam( void )
{
	return &strparam[0];
}

void set_strparam( char * p )
{
	strcpy( strparam, p );

	return;
}

void inicia_sintaxis( void )
{
	int i;

	for( i = 0; i < MAX_ORDERS; i++ )
	{
		obd2_syntax[i].orden = ""; 
		obd2_syntax[i].tipo = 0; 
		obd2_syntax[i].manejador_sintactico = NULL; \
		obd2_syntax[i].desc = "";
	}

	SYN(  0, 1, "SETECU", syn_setecu, "Asigna el valor de la ECU" )
	SYN(  1, 1, "SETLEN", syn_setlen, "Asigna el valor de la longitud de la trama CAN" )
	SYN(  2, 1, "SETSER", syn_setser, "Asigna el valor del servicio OBD2" )
	SYN(  3, 1, "SETPID", syn_setpid, "Asigna el valor del PID" )
	SYN(  4, 1, "SETTMO", syn_settmo, "Asigna el valor del temporizador (en segundos)" )
	SYN(  5, 1, "SETTRC", syn_settrc, "Asigna el valor del nivel de traza" )
	SYN(  6, 1, "SETD0",  syn_setd0, "Asigna el valor del byte 0 de la trama de datos CAN" )
	SYN(  7, 1, "SETD1",  syn_setd1, "Asigna el valor del byte 1 de la trama de datos CAN" )
	SYN(  8, 1, "SETD2",  syn_setd2, "Asigna el valor del byte 2 de la trama de datos CAN" )
	SYN(  9, 1, "SETD3",  syn_setd3, "Asigna el valor del byte 3 de la trama de datos CAN" )
	SYN( 10, 1, "SETD4",  syn_setd4, "Asigna el valor del byte 4 de la trama de datos CAN" )
	SYN( 11, 1, "SETD5",  syn_setd5, "Asigna el valor del byte 5 de la trama de datos CAN" )
	SYN( 12, 1, "SETD6",  syn_setd6, "Asigna el valor del byte 6 de la trama de datos CAN" )
	SYN( 13, 1, "SETD7",  syn_setd7, "Asigna el valor del byte 7 de la trama de datos CAN" )
	SYN( 14, 1, "SETEXT", syn_setext, "ASigna el valor de la dirección extendida de la ECU" )
	SYN( 15, 1, "SEND",   syn_send, "Envía la trama CAN y espera a recibir el número de respuestas indicado" )
	SYN( 16, 0, "HELP",   syn_help, "Muestra la ayuda" )
	SYN( 17, 0, "EXIT",   syn_exit, "Abandona el programa" )
	SYN( 18, 0, "QUIT",   syn_exit, "Abandona el programa" )
	SYN( 19, 0, "SHOWECU", syn_showecu, "Muestra el valor de la ECU" )
	SYN( 20, 0, "SHOWLEN", syn_showlen, "Muestra el valor de la longitud de la trama CAN" )
	SYN( 21, 0, "SHOWSER", syn_showser, "Muestra el valor del servicio OBD2" )
	SYN( 22, 0, "SHOWPID", syn_showpid, "Muestra el valor del PID" )
	SYN( 23, 0, "SHOWTMO", syn_showtmo, "Muestra el valor del temporizador (en segundos)" )
	SYN( 24, 0, "SHOWTRC", syn_showtrc, "Muestra el valor del nivel de traza" )
	SYN( 25, 0, "SHOWD0",  syn_showd0, "Muestra el valor del byte 0 de la trama de datos CAN" )
	SYN( 26, 0, "SHOWD1",  syn_showd1, "Muestra el valor del byte 1 de la trama de datos CAN" )
	SYN( 27, 0, "SHOWD2",  syn_showd2, "Muestra el valor del byte 2 de la trama de datos CAN" )
	SYN( 28, 0, "SHOWD3",  syn_showd3, "Muestra el valor del byte 3 de la trama de datos CAN" )
	SYN( 29, 0, "SHOWD4",  syn_showd4, "Muestra el valor del byte 4 de la trama de datos CAN" )
	SYN( 30, 0, "SHOWD5",  syn_showd5, "Muestra el valor del byte 5 de la trama de datos CAN" )
	SYN( 31, 0, "SHOWD6",  syn_showd6, "Muestra el valor del byte 6 de la trama de datos CAN" )
	SYN( 32, 0, "SHOWD7",  syn_showd7, "Muestra el valor del byte 7 de la trama de datos CAN" )
	SYN( 33, 0, "SHOWEXT", syn_showext, "Muestra el valor de la dirección extendida de la ECU" )
	SYN( 34, 0, "SHOWDTC", syn_showdtc, "Muestra la descripción del DTC indicado" )
	SYN( 35, 1, "CHKPID",  syn_chkpid, "Comprueba si hay el PID esta asignado en el servicio en curso" )
	SYN( 36, 2, "EXECUTE", syn_execute, "Ejecuta el archivo de ordenes" )
	SYN( 37, 0, "STATUS",  syn_status, "Muestra el valor del conjunto de variables" )
	SYN( 38, 1, "SCANSER", syn_scanser, "Solicita todos los PIDs disponibles para el servicio" )
	SYN( 39, 2, "SETBUFF", syn_setbuffer, "Asigna el valor del buffer de envío (en hexadecimal)" )
	SYN( 40, 0, "SHOWBUFF", syn_showbuffer, "Muestra el valor del buffer de envío" )
	SYN( 41, 1, "SENDBUFF", syn_sendbuffer, "Envía el buffer de datos" )
	SYN( 42, 0, "SHOWLENBUFF", syn_showlenbuffer, "Muestra la longitud del buffer de envío" )

	return;
}

int preprocesa_entrada( char * filename )
{
	char ch;
	int lc = 0;
	FILE *fentrada, *fsalida;


	fentrada = fopen( filename, "r" );

	if( fentrada == NULL )
	{
		perror("No se pudo abrir el fichero de entrada");
		return 1;
	}

	fsalida = fopen( ofilename, "w" );

	if( fsalida == NULL )
	{
		perror("No se pudo abrir el fichero de salida");
		return 2;
	}

	while( !feof( fentrada ) )
	{
		fscanf(fentrada, "%c", &ch); 

		/*
		 * Omitir comentarios a continuación
		 */
		if( ch == '#' )
		{
			while( 1 )
			{
				if( EOF == fscanf( fentrada, "%c", &ch ) || ch == '\n' )
					break;	
			}

			ch = '\n';
		}

		/*
		 * Minúscula a mayúscula
		 */
		else if( ch >= 'a' && ch <= 'z' )
		{
			ch -= 32;
			lc++;
		}
		else if( ch != '\n' )
			lc++;

		if( lc > 0 )
			fwrite( &ch, 1, 1, fsalida );

		if( ch == '\n' )
			lc = 0;
	}

	fclose( fentrada );
	fclose( fsalida );

	return 0;
}

int busca_token( char * orden )
{
	int i;

	if( orden == NULL )
		return -1;

	for( i = 0; i < MAX_ORDERS; i++ )
		if( !strcmp( orden, obd2_syntax[i].orden ) )
			return i;

	/* Orden desconocida*/
	
	return -1;
}

int obten_parametro ( char * str )
{
	char *p, *endptr;
	int rc;
	int hex = 0;

	if( str == NULL )
		return 0;

	if( *str == '\0' || *str == '\n' )
		return 0;
	else if( *str == '0' )
	{
		str++;
	
		if( *str == 'x' || *str == 'X' )
		{
			hex = 1;
			str++;
		}
	}

	p = str;

	if( hex == 0 )
	{
		while( isdigit( *str ) )
			str++;

		if( *str == '\0' || *str == '\n' )
		{
			rc = strtol( p, &endptr, 10 );
			return rc;
		}
		else 
			return -4; /* Error sintáctico */
	}
	else
	{
		while( isalnum( *str ) )
			str++;

		if( *str == '\0' || *str == '\n' )
		{
			rc = strtol( p, &endptr, 16 );
			return rc;
		}
		else 
			return -4; /* Error sintáctico */

	}

	/* NOTREACHED */

	return -1;
}


int trata_linea( char * linea )
{
	const char s[2] = " ";
	char *ptr;
	int indice = 0;
	int itk;	
	int rc;
	int param = 0;

	if( *linea == '\0' )
		return 0;

	if( *linea == '!' )
	{
		linea++;
		rc = system( linea );

		return rc;
	}	

	ptr = strtok( linea, s );	

	while( ptr != NULL )
	{
		switch( indice )
		{
			case 0:	
				itk = busca_token( ptr );

				if( itk == -1 )
					/* Order not found */
					return -5;
				break;

			case 1:
				if( obd2_syntax[itk].tipo == 0 || obd2_syntax[itk].tipo == 2 )  
				{
					param = 0;
					set_strparam( ptr );
					/*
					printf("Asignada str param: %s\n", ptr );
					*/
				}
				else
					param = obten_parametro( ptr );
			
				if( param < 0 )
					return param;
			
				break;

			default:	/* Sintax error */
				return -4;
		}
		
		ptr = strtok( NULL, s );

		indice++;
	}

	rc = obd2_syntax[itk].manejador_sintactico( param );

	return rc;
}


int obten_tokens( void )
{
	static char line[256];
	char * buffer = &line[0];
	size_t n = 256;

	FILE *fentrada;

	fentrada = fopen( ofilename, "r" );

	if( fentrada == NULL )
	{
		perror("No se pudo abrir el fichero procesado");
		return 3;
	}

	while( !feof( fentrada ) )
	{
		int rc;

		rc = getline( &buffer, &n, fentrada );
		line[rc-1] = ' '; /* Sustituimos el retorno de carro por un caracter en blanco */

		trata_linea( buffer );
	}

	return 0;

}

int interpreteold( char * filename )
{
        int rc;

       	rc = preprocesa_entrada( filename );

       	if( rc )
               	exit(rc);

       	inicia_sintaxis();

       	rc = obten_tokens();

	return rc;
}

int prepara_linea( char * s )
{
	int mayusculas = 1;

	if( *s == '!' )
		return 0;

	while( *s != '\0' )
	{
		if( *s == ' ' ) 
			mayusculas = 0;
		else if( *s == '#' || *s == '\n')
		{
			*s = '\0';
		}

		/*
		 * Minúscula a mayúscula
		 */
		else if( *s >= 'a' && *s <= 'z' && mayusculas )
		{
			*s -= 32;
		}

		s++;
	}

	return 0;
}


int interprete( char * filename )
{
	FILE *fentrada;
	char str[STR_LEN];
	int rc;

	int linea = 0;

	if( !strcmp( filename, "stdin" ) )
		fentrada = stdin;
	else
		fentrada = fopen( filename, "r" );

	if( fentrada == NULL )
	{
		printf("Fichero: %s\n", filename );
		perror("No se pudo abrir el fichero");
		return 3;
	}

       	inicia_sintaxis();

	while ( fgets(str, STR_LEN, fentrada) )
	{
        	linea++;
		rc = prepara_linea( str );
        	printf("%4d: %s\n", linea, str); /* borrame */

		rc = trata_linea( str );

		if( rc < 0 )
			printf("error en el procesamiento de la linea: %s, Error: %d\n", str, rc );
    	}

    	fclose(fentrada);

	return 0;
}
