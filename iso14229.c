
static char * respuestas_14229[] = {
	"Respuesta positiva",		/* 0x00 */
	"Reservada ISO/SAE",		/* 0x01 */
	"Reservada ISO/SAE",		/* 0x02 */
	"Reservada ISO/SAE",		/* 0x03 */
	"Reservada ISO/SAE",		/* 0x04 */
	"Reservada ISO/SAE",		/* 0x05 */
	"Reservada ISO/SAE",		/* 0x06 */
	"Reservada ISO/SAE",		/* 0x07 */
	"Reservada ISO/SAE",		/* 0x08 */
	"Reservada ISO/SAE",		/* 0x09 */
	"Reservada ISO/SAE",		/* 0x0A */
	"Reservada ISO/SAE",		/* 0x0B */
	"Reservada ISO/SAE",		/* 0x0C */
	"Reservada ISO/SAE",		/* 0x0D */
	"Reservada ISO/SAE",		/* 0x0E */
	"Reservada ISO/SAE",		/* 0x0F */
	"Rechazo general",		/* 0x10 */
	"Servicio no soportado",	/* 0x11 */
	"SubFunción no soportada",	/* 0x12 */
	"Longitud de mensaje incorrecta o formato inválido",	/* 0x13 */
	"Respuesta demasiado larga",	/* 0x14 */
	"Reservada ISO/SAE",		/* 0x15 */
	"Reservada ISO/SAE",		/* 0x16 */
	"Reservada ISO/SAE",		/* 0x17 */
	"Reservada ISO/SAE",		/* 0x18 */
	"Reservada ISO/SAE",		/* 0x19 */
	"Reservada ISO/SAE",		/* 0x20 */
	"Servidor ocupado",		/* 0x21 */
	"Condiciones no correctas",	/* 0x22 */
	"Reservada ISO/SAE",		/* 0x23 */
	"Solicitud en secuencia incorrecta",	/* 0x24 */
	"Sin respuesta desde el componente de la subred",	/* 0x25 */
	"Fallo previo impide la ejecución de esta acción",	/* 0x26 */
	"Reservada ISO/SAE",		/* 0x27 */
	"Reservada ISO/SAE",		/* 0x28 */
	"Reservada ISO/SAE",		/* 0x29 */
	"Reservada ISO/SAE",		/* 0x30 */
	"Solicitud fuera de rango",	/* 0x31 */
	"Reservada ISO/SAE",		/* 0x32 */
	"Acceso denegado",		/* 0x33 */
	"Reservada ISO/SAE",		/* 0x34 */
	"Clave inválida",		/* 0x35 */
	"Excedido el número de intentos",	/* 0x36 */
	"El tiempo de demora requerido no ha expirado",	/* 0x37 */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x38 */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x39 */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x40 */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x41 */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x42 */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x43 */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x44 */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x45 */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x46 */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x47 */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x48 */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x49 */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x4A */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x4B */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x4C */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x4D */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x4E */
	"Reservado Seguridad de enlace de datos extendido ISO 15864",		/* 0x4F */
	"Reservada ISO/SAE",		/* 0x50 */
	"Reservada ISO/SAE",		/* 0x51 */
	"Reservada ISO/SAE",		/* 0x52 */
	"Reservada ISO/SAE",		/* 0x53 */
	"Reservada ISO/SAE",		/* 0x54 */
	"Reservada ISO/SAE",		/* 0x55 */
	"Reservada ISO/SAE",		/* 0x56 */
	"Reservada ISO/SAE",		/* 0x57 */
	"Reservada ISO/SAE",		/* 0x58 */
	"Reservada ISO/SAE",		/* 0x59 */
	"Reservada ISO/SAE",		/* 0x5B */
	"Reservada ISO/SAE",		/* 0x5C */
	"Reservada ISO/SAE",		/* 0x5D */
	"Reservada ISO/SAE",		/* 0x5E */
	"Reservada ISO/SAE",		/* 0x5F */
	"Reservada ISO/SAE",		/* 0x60 */
	"Reservada ISO/SAE",		/* 0x61 */
	"Reservada ISO/SAE",		/* 0x62 */
	"Reservada ISO/SAE",		/* 0x63 */
	"Reservada ISO/SAE",		/* 0x64 */
	"Reservada ISO/SAE",		/* 0x65 */
	"Reservada ISO/SAE",		/* 0x66 */
	"Reservada ISO/SAE",		/* 0x67 */
	"Reservada ISO/SAE",		/* 0x68 */
	"Reservada ISO/SAE",		/* 0x69 */
	"Reservada ISO/SAE",		/* 0x6A */
	"Reservada ISO/SAE",		/* 0x6B */
	"Reservada ISO/SAE",		/* 0x6C */
	"Reservada ISO/SAE",		/* 0x6D */
	"Reservada ISO/SAE",		/* 0x6E */
	"Reservada ISO/SAE",		/* 0x6F */
	"Subida o bajada de datos no acpetada",		/* 0x70 */
	"Transferencia de datos supendida",		/* 0x71 */
	"Fallo general del programa",			/* 0x72 */
	"Contador de secuencia de bloque incorrecto",	/* 0x73 */
	"Reservada ISO/SAE",		/* 0x74 */
	"Reservada ISO/SAE",		/* 0x75 */
	"Reservada ISO/SAE",		/* 0x76 */
	"Reservada ISO/SAE",		/* 0x77 */
	"Solicitud correctamente recibida - Respuesta pendiente",		/* 0x78 */
	"Reservada ISO/SAE",		/* 0x79 */
	"Reservada ISO/SAE",		/* 0x7A */
	"Reservada ISO/SAE",		/* 0x7B */
	"Reservada ISO/SAE",		/* 0x7C */
	"Reservada ISO/SAE",		/* 0x7D */
	"Subfunción no soportada en la sesión activa",		/* 0x7E */
	"Servicio no soportado en la sesión activa",		/* 0x7F */
	"Reservada ISO/SAE",		/* 0x80 */
	"RPM demasiado altas",		/* 0x81 */
	"RPM demasiado bajas",		/* 0x82 */
	"El motor está funcionando",	/* 0x83 */
	"El motor no está funcionando",	/* 0x84 */
	"El tiempo de funcionamiento del motor es demasiado bajo",	/* 0x85 */
	"Temperatura demasiado elevada",/* 0x86 */
	"Temperatura demasiado baja",	/* 0x87 */
	"Velocidad del vehículo demasiado alta",	/* 0x88 */
	"Velocidad del vehículo demasiado baja",	/* 0x89 */
	"Pedal del acelerador demaisado alto",	/* 0x8A */
	"Pedal del acelerador demaisado bajo",	/* 0x8B */
	"La caja de cambios no está en punto muerto",	/* 0x8C */
	"La caja de cambios no tiene una marcha engranada",	/* 0x8D */
	"Reservada ISO/SAE",		/* 0x8E */
	"El pedal del freno no está pisado",		/* 0x8F */
	"El freno de mano no está activado",		/* 0x90 */
	"Convertidor de par del embrague bloqueado",	/* 0x91 */
	"Voltaje demasiado elevado",	/* 0x92 */
	"Voltaje demasiado bajo",	/* 0x93 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0x94 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0x95 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0x96 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0x97 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0x98 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0x99 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0x9A */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0x9B */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0x9C */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0x9D */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0x9E */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0x9F */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xA0 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xA1 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xA2 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xA3 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xA4 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xA5 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xA6 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xA7 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xA8 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xA9 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xAA */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xAB */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xAC */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xAD */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xAE */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xB0 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xB1 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xB2 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xB3 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xB4 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xB5 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xB6 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xB7 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xB8 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xB9 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xBA */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xBB */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xBC */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xBD */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xBE */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xBF */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xC0 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xC1 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xC2 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xC3 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xC4 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xC5 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xC6 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xC7 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xC8 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xC9 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xCA */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xCB */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xCC */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xCD */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xCE */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xCF */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xCF */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xD0 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xD1 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xD2 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xD3 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xD4 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xD5 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xD6 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xD7 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xD8 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xD9 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xDA */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xDB */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xDC */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xDD */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xDE */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xDF */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xE0 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xE1 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xE2 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xE3 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xE4 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xE5 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xE6 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xE7 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xE8 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xE9 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xEA */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xEB */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xEC */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xED */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xEE */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xEF */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xF0 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xF1 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xF2 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xF3 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xF4 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xF5 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xF6 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xF7 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xF8 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xF9 */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xFA */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xFB */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xFC */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xFD */
	"Reservado por ISO 14229 para condiciones específicas no correctas",		/* 0xFE */
	"Reservada ISO/SAE"		/* 0xFF */ 
};

char * get_respuesta_14229( unsigned int id )
{
	return respuestas_14229[ id ];
}
