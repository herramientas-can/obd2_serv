/*
    Copyright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
   
    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */

#ifndef MP_OBD2_MODE6_H
#define MP_OBD2_MODE6_H

#ifndef MP_OBD2_H
include "obd2.h"
#endif 	/* MP_OBD2_H */

/*
 *  Basado en el anexo D de ISO 15765-4
 *
 *  Definiciones de OBDMIDs (On-Board Diagnostic Monitor ID) para el servicio 0x06
 *
 */


FDEF( 0x06, 0x00, 4, "IDs de monitores OBD soportados [01 - 20]", "", obd2_pids_soportados )
FDEF( 0x06, 0x01, -1, "Monitor del sensor de oxígeno - Banco 1 – Sensor 1", "", obd2_s6 )
FDEF( 0x06, 0x02, -1, "Monitor del sensor de oxígeno - Banco 1 – Sensor 2", "", obd2_s6 )
FDEF( 0x06, 0x03, -1, "Monitor del sensor de oxígeno - Banco 1 – Sensor 3", "", obd2_s6 )
FDEF( 0x06, 0x04, -1, "Monitor del sensor de oxígeno - Banco 1 – Sensor 4", "", obd2_s6 )
FDEF( 0x06, 0x05, -1, "Monitor del sensor de oxígeno - Banco 2 – Sensor 1", "", obd2_s6 )
FDEF( 0x06, 0x06, -1, "Monitor del sensor de oxígeno - Banco 2 – Sensor 2", "", obd2_s6 )
FDEF( 0x06, 0x07, -1, "Monitor del sensor de oxígeno - Banco 2 – Sensor 3", "", obd2_s6 )
FDEF( 0x06, 0x08, -1, "Monitor del sensor de oxígeno - Banco 2 – Sensor 4", "", obd2_s6 )
FDEF( 0x06, 0x09, -1, "Monitor del sensor de oxígeno - Banco 3 – Sensor 1", "", obd2_s6 )
FDEF( 0x06, 0x0A, -1, "Monitor del sensor de oxígeno - Banco 3 – Sensor 2", "", obd2_s6 )
FDEF( 0x06, 0x0B, -1, "Monitor del sensor de oxígeno - Banco 3 – Sensor 3", "", obd2_s6 )
FDEF( 0x06, 0x0C, -1, "Monitor del sensor de oxígeno - Banco 3 – Sensor 4", "", obd2_s6 )
FDEF( 0x06, 0x0D, -1, "Monitor del sensor de oxígeno - Banco 4 – Sensor 1", "", obd2_s6 )
FDEF( 0x06, 0x0E, -1, "Monitor del sensor de oxígeno - Banco 4 – Sensor 2", "", obd2_s6 )
FDEF( 0x06, 0x0F, -1, "Monitor del sensor de oxígeno - Banco 4 – Sensor 3", "", obd2_s6 )
FDEF( 0x06, 0x10, -1, "Monitor del sensor de oxígeno - Banco 4 – Sensor 4", "", obd2_s6 )
FDEF( 0x06, 0x12, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x13, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x14, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x15, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x16, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x17, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x18, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x19, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x1A, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x1B, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x1C, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x1D, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x1E, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x1F, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x20, 4, "IDs de monitores OBD soportados [21 - 40]", "", obd2_pids_soportados )
FDEF( 0x06, 0x21, -1, "Monitor del catalizador - Banco 1", "", obd2_s6 )
FDEF( 0x06, 0x22, -1, "Monitor del catalizador - Banco 2", "", obd2_s6 )
FDEF( 0x06, 0x23, -1, "Monitor del catalizador - Banco 3", "", obd2_s6 )
FDEF( 0x06, 0x24, -1, "Monitor del catalizador - Banco 4", "", obd2_s6 )
FDEF( 0x06, 0x25, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x26, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x27, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x28, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x29, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x30, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x31, -1, "Monitor EGR - Banco 1", "", obd2_s6 )
FDEF( 0x06, 0x32, -1, "Monitor EGR - Banco 2", "", obd2_s6 )
FDEF( 0x06, 0x33, -1, "Monitor EGR - Banco 3", "", obd2_s6 )
FDEF( 0x06, 0x34, -1, "Monitor EGR - Banco 4", "", obd2_s6 )
FDEF( 0x06, 0x35, -1, "Monitor VVT - Banco 1", "", obd2_s6 )
FDEF( 0x06, 0x36, -1, "Monitor VVT - Banco 2", "", obd2_s6 )
FDEF( 0x06, 0x37, -1, "Monitor VVT - Banco 3", "", obd2_s6 )
FDEF( 0x06, 0x38, -1, "Monitor VVT - Banco 4", "", obd2_s6 )
FDEF( 0x06, 0x39, -1, "Monitor EVAP (Cap Off / 0.150”)", "", obd2_s6 )
FDEF( 0x06, 0x3A, -1, "Monitor EVAP (0.090“)", "", obd2_s6 )
FDEF( 0x06, 0x3B, -1, "Monitor EVAP (0.040”)", "", obd2_s6 )
FDEF( 0x06, 0x3C, -1, "Monitor EVAP (0.020“)", "", obd2_s6 )
FDEF( 0x06, 0x3D, -1, "Monitor del flujo de purgado", "", obd2_s6 )
FDEF( 0x06, 0x3E, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x3F, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x40, 4, "IDs de monitores OBD soportados [41 - 60]", "", obd2_pids_soportados )
FDEF( 0x06, 0x41, -1, "Monitor del calentador del sensor de oxígeno - Banco 1 – Sensor 1", "", obd2_s6 )
FDEF( 0x06, 0x42, -1, "Monitor del calentador del sensor de oxígeno - Banco 1 – Sensor 2", "", obd2_s6 )
FDEF( 0x06, 0x43, -1, "Monitor del calentador del sensor de oxígeno - Banco 1 – Sensor 3", "", obd2_s6 )
FDEF( 0x06, 0x44, -1, "Monitor del calentador del sensor de oxígeno - Banco 1 – Sensor 4", "", obd2_s6 )
FDEF( 0x06, 0x45, -1, "Monitor del calentador del sensor de oxígeno - Banco 2 – Sensor 1", "", obd2_s6 )
FDEF( 0x06, 0x46, -1, "Monitor del calentador del sensor de oxígeno - Banco 2 – Sensor 2", "", obd2_s6 )
FDEF( 0x06, 0x47, -1, "Monitor del calentador del sensor de oxígeno - Banco 2 – Sensor 3", "", obd2_s6 )
FDEF( 0x06, 0x48, -1, "Monitor del calentador del sensor de oxígeno - Banco 2 – Sensor 4", "", obd2_s6 )
FDEF( 0x06, 0x49, -1, "Monitor del calentador del sensor de oxígeno - Banco 3 – Sensor 1", "", obd2_s6 )
FDEF( 0x06, 0x4A, -1, "Monitor del calentador del sensor de oxígeno - Banco 3 – Sensor 2", "", obd2_s6 )
FDEF( 0x06, 0x4B, -1, "Monitor del calentador del sensor de oxígeno - Banco 3 – Sensor 3", "", obd2_s6 )
FDEF( 0x06, 0x4C, -1, "Monitor del calentador del sensor de oxígeno - Banco 3 – Sensor 4", "", obd2_s6 )
FDEF( 0x06, 0x4D, -1, "Monitor del calentador del sensor de oxígeno - Banco 4 – Sensor 1", "", obd2_s6 )
FDEF( 0x06, 0x4E, -1, "Monitor del calentador del sensor de oxígeno - Banco 4 – Sensor 2", "", obd2_s6 )
FDEF( 0x06, 0x4F, -1, "Monitor del calentador del sensor de oxígeno - Banco 4 – Sensor 3", "", obd2_s6 )
FDEF( 0x06, 0x50, -1, "Monitor del calentador del sensor de oxígeno - Banco 4 – Sensor 4", "", obd2_s6 )
FDEF( 0x06, 0x51, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x52, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x53, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x54, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x55, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x56, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x57, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x58, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x59, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x5A, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x5B, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x5C, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x5D, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x5E, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x5F, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x60, 4, "IDs de monitores OBD soportados [61 - 80]", "", obd2_pids_soportados )
FDEF( 0x06, 0x61, -1, "Monitor del calentador del catalizador - Banco 1", "", obd2_s6 )
FDEF( 0x06, 0x62, -1, "Monitor del calentador del catalizador - Banco 2", "", obd2_s6 )
FDEF( 0x06, 0x63, -1, "Monitor del calentador del catalizador - Banco 3", "", obd2_s6 )
FDEF( 0x06, 0x64, -1, "Monitor del calentador del catalizador - Banco 4", "", obd2_s6 )
FDEF( 0x06, 0x65, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x66, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x67, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x68, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x69, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x6A, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x6B, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x6C, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x6D, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x6E, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x6F, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x70, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x71, -1, "Monitor de aire secundario 1", "", obd2_s6 )
FDEF( 0x06, 0x72, -1, "Monitor de aire secundario 2", "", obd2_s6 )
FDEF( 0x06, 0x73, -1, "Monitor de aire secundario 3", "", obd2_s6 )
FDEF( 0x06, 0x74, -1, "Monitor de aire secundario 4", "", obd2_s6 )
FDEF( 0x06, 0x75, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x76, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x77, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x78, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x79, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x7A, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x7B, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x7C, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x7D, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x7E, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x7F, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x80, 4, "IDs de monitores OBD soportados [81 - A0]", "", obd2_pids_soportados )
FDEF( 0x06, 0x81, -1, "Monitor del sistema de combustible - Banco 1", "", obd2_s6 )
FDEF( 0x06, 0x82, -1, "Monitor del sistema de combustible - Banco 2", "", obd2_s6 )
FDEF( 0x06, 0x83, -1, "Monitor del sistema de combustible - Banco 3", "", obd2_s6 )
FDEF( 0x06, 0x84, -1, "Monitor del sistema de combustible - Banco 4", "", obd2_s6 )
FDEF( 0x06, 0x85, -1, "Monitor del control de aumento de presión - Banco 1", "", obd2_s6 )
FDEF( 0x06, 0x86, -1, "Monitor del control de aumento de presión - Banco 2", "", obd2_s6 )
FDEF( 0x06, 0x87, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x88, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x89, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x8A, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x8B, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x8C, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x8D, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x8E, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x8F, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x90, -1, "Monitor de absorción de NOx - Banco 1", "", obd2_s6 )
FDEF( 0x06, 0x91, -1, "Monitor de absorción de NOx - Banco 2", "", obd2_s6 )
FDEF( 0x06, 0x92, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x93, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x94, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x95, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x96, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x97, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x98, -1, "NOx Monitor del catalizador - Banco 1", "", obd2_s6 )
FDEF( 0x06, 0x99, -1, "NOx Monitor del catalizador - Banco 2", "", obd2_s6 )
FDEF( 0x06, 0x9A, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x9B, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x9C, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x9D, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x9E, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0x9F, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xA0, 4, "IDs de monitores OBD soportados [A1 - C0]", "", obd2_pids_soportados )
FDEF( 0x06, 0xA1, -1, "Monitor general de fallos de encendido", "", obd2_s6 )
FDEF( 0x06, 0xA2, -1, "Datos de fallo de encendido del cilindro 1", "", obd2_s6 )
FDEF( 0x06, 0xA3, -1, "Datos de fallo de encendido del cilindro 2", "", obd2_s6 )
FDEF( 0x06, 0xA4, -1, "Datos de fallo de encendido del cilindro 3", "", obd2_s6 )
FDEF( 0x06, 0xA5, -1, "Datos de fallo de encendido del cilindro 4", "", obd2_s6 )
FDEF( 0x06, 0xA6, -1, "Datos de fallo de encendido del cilindro 5", "", obd2_s6 )
FDEF( 0x06, 0xA7, -1, "Datos de fallo de encendido del cilindro 6", "", obd2_s6 )
FDEF( 0x06, 0xA8, -1, "Datos de fallo de encendido del cilindro 7", "", obd2_s6 )
FDEF( 0x06, 0xA9, -1, "Datos de fallo de encendido del cilindro 8", "", obd2_s6 )
FDEF( 0x06, 0xAA, -1, "Datos de fallo de encendido del cilindro 9", "", obd2_s6 )
FDEF( 0x06, 0xAB, -1, "Datos de fallo de encendido del cilindro 10", "", obd2_s6 )
FDEF( 0x06, 0xAC, -1, "Datos de fallo de encendido del cilindro 11", "", obd2_s6 )
FDEF( 0x06, 0xAD, -1, "Datos de fallo de encendido del cilindro 12", "", obd2_s6 )
FDEF( 0x06, 0xAE, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xAF, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xB0, -1, "Monitor del filtro de partículas - Banco 1", "", obd2_s6 )
FDEF( 0x06, 0xB1, -1, "Monitor del filtro de partículas - Banco 2", "", obd2_s6 )
FDEF( 0x06, 0xB2, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xB3, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xB4, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xB5, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xB6, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xB7, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xB8, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xB9, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xBA, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xBB, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xBC, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xBD, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xBE, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xBF, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xC0, 4, "IDs de monitores OBD soportados [C1 - E0]", "", obd2_pids_soportados )
FDEF( 0x06, 0xC1, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xC2, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xC3, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xC4, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xC5, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xC6, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xC7, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xC8, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xC9, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xCA, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xCB, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xCC, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xCD, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xCE, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xCF, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xD0, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xD1, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xD2, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xD3, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xD4, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xD5, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xD6, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xD7, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xD8, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xD9, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xDA, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xDB, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xDC, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xDD, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xDE, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xDF, -1, "Reservado ISO/SAE", "", obd2_s6 )
FDEF( 0x06, 0xE0, 4, "IDs de monitores OBD soportados [E1 - 100]", "", obd2_pids_soportados )
FDEF( 0x06, 0xE1, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xE2, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xE3, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xE4, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xE5, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xE6, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xE7, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xE8, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xE9, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xEA, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xEB, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xEC, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xED, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xEE, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xEF, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xF0, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xF1, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xF2, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xF3, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xF4, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xF5, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xF6, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xF7, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xF8, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xF9, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xFA, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xFB, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xFC, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xFD, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xFE, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )
FDEF( 0x06, 0xFF, -1, "OBDMIDs definido por el fabricante del vehículo", "", obd2_s6 )

#endif /* MP_OBD2_MODE6_H */
