/*
    Copyright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
   
    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <endian.h>
#include <signal.h>
#include <errno.h>

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <net/if.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "can.h"

/*
 *  CAN connection variables
 */
static struct sockaddr_can addr;
static struct ifreq ifr;
static int sockfd;

static unsigned int can_vtimeout;
static unsigned int can_verbose;
static unsigned int ecu_address;
static unsigned int default_param;

static void can_representa_trama( char * prefijo, struct can_frame *trama )
{
	printf("%s %X: %02X %02X %02X %02X %02X %02X %02X %02X\n", prefijo, trama->can_id, trama->data[0], trama->data[1], \
			trama->data[2], trama->data[3], trama->data[4], trama->data[5], trama->data[6], trama->data[7] );

	return;
}

void set_default_param( unsigned int param )
{
	default_param = param;
}

unsigned int get_default_param( void )
{
	return default_param;
}

void set_ecu_address( unsigned int addr )
{
	ecu_address = addr;
}

unsigned int get_ecu_address( void )
{
	return ecu_address;
}

void can_set_verbose( unsigned int verb )
{
	can_verbose = verb;
}

unsigned int can_get_verbose( void )
{
	return can_verbose;
}

void can_set_timeout( unsigned int tmot )
{
	can_vtimeout = tmot;
}

unsigned int can_get_timeout( void )
{
	return can_vtimeout;
}

void can_timeout( int sig )
{
	puts("CAN timeout!");
	exit(-5);
}

int can_abre_socket( void )
{

	/*
         * Abrimos el socket, en modo RAW
         */
        sockfd = socket(PF_CAN, SOCK_RAW, CAN_RAW);

        if (sockfd < 0)
        {
                perror("Error abriendo socket");
                return -1;
        }

	return 0;
}

int can_cierra_socket( void )
{
	int rc;

	rc = close( sockfd );

	return rc;
}


int can_obd_filtro( void )
{	
	int rc;
	struct can_filter filter[1];

	/*
	 * Definimos un filtro para recibir sólo los CAN ID mayores de 0x700, que suponemos
	 * trafico con las ECUs.
	 */
        filter[0].can_id   = 0x700;
        filter[0].can_mask = 0x700;

        rc = setsockopt( sockfd, SOL_CAN_RAW, CAN_RAW_FILTER, &filter, sizeof(filter));

        if( -1 == rc)
	{
            perror("setsockopt filtro");
	    return -1;
        }

	return 0;
}

int can_referencia_interfaz( const char * interface )
{
	int rc;

        /*
         *  Obtenemos la referencia al interfaz de red 
         */
    	strcpy(ifr.ifr_name, interface );

        rc = ioctl(sockfd, SIOCGIFINDEX, &ifr);

        if( rc < 0)
        {
                perror("Error configurando socket");
                return -2;
        }

	return 0;

}

int can_enlaza_interfaz( void )
{
	int rc;
	int recv_own_msgs;

        /*
         * Enlazamos el socket con el interfaz
         */
        addr.can_family = AF_CAN;
        addr.can_ifindex = ifr.ifr_ifindex;

        rc = bind(sockfd, (struct sockaddr *)&addr, sizeof(addr));

        if( rc < 0)
        {
                perror("Error enlazando socket e interfaz");
                return -3;
        }

	/*
	 * Desactivamos la recepción de nuestros propios mensajes
	 */
        recv_own_msgs = 0; /* 0 = disabled (default), 1 = enabled */

        rc = setsockopt(sockfd, SOL_CAN_RAW, CAN_RAW_RECV_OWN_MSGS, &recv_own_msgs, sizeof(recv_own_msgs));

        if( -1 == rc)
	{
            perror("setsockopt recepción mensajes");
	    return -1;
        }

	return 0;
}

int can_escribe_trama( struct can_frame * frame )
{
	int rc;

	alarm(can_vtimeout);	/* Fijamos el timeout */

	if( can_get_verbose() > 0 )
		can_representa_trama( ">>", frame );

	rc = write(sockfd, frame, sizeof(struct can_frame) );
		       
	if (rc != sizeof(struct can_frame) )
	{
		perror("Error al escribir en el bus");
		alarm(0);
		return -1;
	}


	return rc;
}

int can_lee_trama( struct can_frame * frame )
{
	int rc;

        rc = read(sockfd, frame, CAN_MTU);

	alarm(0);	/* desactivamos el timeout */

	if( can_get_verbose() > 0 )
		can_representa_trama( "<<", frame );

        return rc;
}
