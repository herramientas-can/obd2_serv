/*
    Copyright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
   
    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */

#ifndef MP_OBD2_MODE9_H
#define MP_OBD2_MODE9_H

#ifndef MP_OBD2_H
include "obd2.h"
#endif /* MP_OBD2_H */

FDEF( 0x09, 0x00, 4, "PIDs implementados [01 - 20]", "", obd2_pids_soportados )
DEF( 0x09, 0x1, 1, "VIN Message Count in PID 02. Only for ISO 9141-2, ISO 14230-4 and SAE J1850." )
FDEF( 0x09, 0x2, -1, "Número de identificación del vehículo (VIN) ...................", "", obd2_s9_vin )
DEF( 0x09, 0x3, 1, "Calibration ID message count for PID 04. Only for ISO 9141-2, ISO 14230-4 and SAE J1850." )
FDEF( 0x09, 0x4, -1, "Identificador de la calibración ...............................", "", obd2_s9_vin )
DEF( 0x09, 0x5, 1, "Calibration verification numbers (CVN) message count for PID 06. Only for ISO 9141-2, ISO 14230-4 and SAE J1850." )
FDEF( 0x09, 0x6, -1, "Número de verificación de la calibración (CVN) ................", "", obd2_s9_cvn )
DEF( 0x09, 0x7, 1, "In-use performance tracking message count for PID 08 and 0B. Only for ISO 9141-2, ISO 14230-4 and SAE J1850." )
FDEF( 0x09, 0x8, -1, "Seguimiento del rendimiento en vehículos de encendido provocado", "", obd2_s9_tracking )
DEF( 0x09, 0x9, 1, "ECU name message count for PID 0A" )
DEF( 0x09, 0xA, 20, "Nombre de la ECU" )
DEF( 0x09, 0xB, 4, "Seguimiento del rendimiento en vehículos de encendido por compresión" )

#endif /* MP_OBD2_MODE9_H */
