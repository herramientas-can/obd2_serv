/*
    Copyright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
   
    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */

#ifndef MP_OBD2_MODE8_H
#define MP_OBD2_MODE8_H

#ifndef MP_OBD2_H
include "obd2.h"
#endif 	/* MP_OBD2_H */

FDEF( 0x08, 0x00, 4, "PIDs implementados [01 - 20]", "", obd2_pids_soportados )
FDEF( 0x08, 0x20, 4, "PIDs implementados [21 - 40]", "", obd2_pids_soportados )
FDEF( 0x08, 0x40, 4, "PIDs implementados [41 - 60]", "", obd2_pids_soportados )
FDEF( 0x08, 0x60, 4, "PIDs implementados [61 - 80]", "", obd2_pids_soportados )
FDEF( 0x08, 0x80, 4, "PIDs implementados [81 - A0]", "", obd2_pids_soportados )
FDEF( 0x08, 0xA0, 4, "PIDs implementados [A1 - C0]", "", obd2_pids_soportados )
FDEF( 0x08, 0xC0, 4, "PIDs implemantados [C1 - E0]", "", obd2_pids_soportados )
FDEF( 0x08, 0xE0, 4, "PIDs implemantados [E1 - 100]", "", obd2_pids_soportados )

#endif /* MP_OBD2_MODE8_H */
