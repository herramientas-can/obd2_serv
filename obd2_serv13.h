/*
    Copyright 2021 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
   
    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */

#ifndef MP_OBD2_MODE13_H
#define MP_OBD2_MODE13_H

#ifndef MP_OBD2_H
include "obd2.h"
#endif 	/* MP_OBD2_H */

FDEF( 0x13, 0x81, -1, "DTCs actuales", "", obd2_obten_dtcs )
FDEF( 0x13, 0x82, -1, "DTCs históricos", "", obd2_obten_dtcs )

#endif /* MP_OBD2_MODE13_H */
