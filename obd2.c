/*
    Copyright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
   
    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */

/*
 * Control de Cambios
 *
 * 12/12/2019	mapv	Escrito
 * 15/01/2020	mapv	Cambios en la función obd2_solicita_servicio_pid() para invocar correctamente a los servicios 0x03, 0x04, 0x07 y 0x0A
 * 17/01/2020	mapv	Añadida obd2_dtc
 * 19/01/2020	mapv	Modificadas funciones del servicio 0x01 para que sean validas también para el servicio 0x02. Se elimina obd2_serv2.h
 * 05/03/2021	mapv	Se añade soporte a más servicios UDS.
 * 25/08/2021	mapv	Se añade soporte a direcciones extendidas.
 * 13/09/2021	mapv	Se añade validación de DTCs para el servicio 0x13 conforme a los valores que manda Toyota.
 * 23/10/2021	mapv	Se añade capacidad de crear guiones de ordenees
 * 29/12/2021	mapv	Se añade soporte ISO TP a las tramas de envío mediante la posibilidad de envío de un buffer.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <limits.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>

#include <inttypes.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "can.h"
#include "obd2.h"
#include "iso14229.h"

LOCAL int pid;

LOCAL struct trama {
	unsigned int def;
	unsigned int dato;
} obd2_trama[8];

LOCAL unsigned int tr_long;
LOCAL unsigned int tr_serv;
LOCAL unsigned int tr_pid;


char * obd2_tipo_obd[ OBD_TYPES ] = {
	"Reservado",
	"OBD-II definido por la CARB (California Air Resources Board)",
	"OBD definido por la EPA (Environmental Protection Agency)",
	"OBD y OBD-II",
	"OBD-I",
	"No cumple OBD",
	"EOBD (Europa)",
	"EOBD y OBD-II",
	"EOBD y OBD",
	"EOBD, OBD and OBD II",
	"JOBD (Japón)",
	"JOBD y OBD II",
	"JOBD y EOBD",
	"JOBD, EOBD, y OBD II",
	"Reservado",
	"Reservado",
	"Reservado",
	"Diagnosis del fabricante del motor (EMD)",
	"Diagnosis mejorada del fabricante del motor Enhanced (EMD+)",
	"Heavy Duty On-Board Diagnostics (Child/Partial) (HD OBD-C)",
	"Heavy Duty On-Board Diagnostics (HD OBD)",
	"World Wide Harmonized OBD (WWH OBD)",
	"Reservado",
	"Heavy Duty Euro OBD Etapa I sin NOx control (HD EOBD-I)",
	"Heavy Duty Euro OBD Etapa I con NOx control (HD EOBD-I N)",
	"Heavy Duty Euro OBD Etapa II sin NOx control (HD EOBD-II)",
	"Heavy Duty Euro OBD Etapa II con NOx control (HD EOBD-II N)",
	"Reservado",
	"Brasil OBD Fase 1 (OBDBr-1)",
	"Brasil OBD Fase 2 (OBDBr-2)",
	"Corea OBD (KOBD)",
	"India OBD I (IOBD I)",
	"India OBD II (IOBD II)",
	"Heavy Duty Euro OBD Etapa VI (HD EOBD-IV)",
	"Reservado" };

char * obd2_tipo_combustible [OBD_FUEL] = { "No disponible",
	"Gasolina", 
	"Metanol",
	"Etanol",
	"Diesel",
	"LPG",
	"CNG",
	"Propano",
	"Eléctrico",
	"Bifuel usando Gasolina",
	"Bifuel usando Metanol",
	"Bifuel usando Etanol",
	"Bifuel usando LPG",
	"Bifuel usando CNG",
	"Bifuel usando Propano",
	"Bifuel usando Electricidad",
	"Bifuel usando el motor eléctrico y el de combustion",
	"Hibrido gasolina",
	"Hibrido Etanol",
	"Hibrido Diesel",
	"Hibrido Eléctrico",
	"Híbrido usando el motor eléctrico y el de combustion",
	"Hibrido Regenerativo",
	"Bifuel usando diesel " };

static int ext_address;

void set_ext_address( int addr )
{
        ext_address = addr;
}

int get_ext_address( void )
{
        return ext_address;
}

LOCAL int obd2_solicita_servicio_pid( int servicio )
{
	struct can_frame frame;
	int rc;
	int offset;

	if( ext_address == NO_EXT_ADDRESS )
		offset = 0;
	else
		offset = 1;

	/*
	 * Configuramos la trama CAN a enviar ...
	 */
	frame.can_id    = get_ecu_address();
	frame.can_dlc   = 8;


	if( ext_address == NO_EXT_ADDRESS )
	{
		/* En frame.data[0] va la longitud de la trama */

        	frame.data[1]   = servicio;
        	frame.data[2]   = 0x00;
	}
	else
	{
		/* En frame.data[1] va la longitud de la trama */

		frame.data[0]	= ext_address;
        	frame.data[2]   = servicio;
	}

        frame.data[3]   = 0x00;
        frame.data[4]   = 0x00;
        frame.data[5]   = 0x00;
        frame.data[6]   = 0x00;
        frame.data[7]   = 0x00;

	switch ( servicio )
	{
		case 0x03:	/* Show stored Diagnostic Trouble Codes */
		case 0x07:	/* Show pending Diagnostic Trouble Codes (detected during current or last driving cycle */
		case 0x0A:	/* Permanent Diagnostic Trouble Codes (DTCs) (Cleared DTCs) */
		case 0x17: 	/* Read Status Of Diagnostic Trouble Codes */
		case 0x20: 	/* Stop Diagnostic Session */

        		frame.data[0+offset]   = 0x01;		/* 0x01 -> longitud 1 byte */
			break;

		case 0x01: 	/* Show current data */
		case 0x05:	/* Test Results, oxygen sensor monitoring (non CAN only) */	
		case 0x06:	/* Test Results, other component/system monitoring (oxygen sensor monitoring for CAN only */
		case 0x08:	/* Control operation of on-board component/system */
		case 0x09:	/* Request Vehicle information */		
		case 0x10:	/* Diagnostic Session Control */	
		case 0x12:	/* Read Freeze Frame Data */	
		case 0x13: 	/* Read Diagnostic Trouble Codes */
		case 0x1A:	/* Read Ecu identification Service */
		case 0x21:	/* Read Data by Local Identifier */
		case 0x30:	/* Input Output Control By Local Identifier */
		case 0x31:	/* Start Routine By Local Identifier */
		case 0x32:	/* Stop Routine By Local Identifier */
		case 0x33:	/* Request Routine Results By Local Identifier */
		case 0x3E:	/* Tester Present: parametros: 0x01 para requerir respuesta, 0x02 para no hacerlo */

			if( pid > 0xFF )
			{
				pid = pid & 0xFF;
				printf("ID mayor de 0xFF, usando byte menos significativo: 0x%X como ID\n", pid );
			}

        		frame.data[0+offset]   = 0x02;		/* 0x02 -> longitud 2 bytes */
        		frame.data[2+offset]   = pid;
			break;

		case 0x02:	/* Show Freeze Frame Data */
			if( pid > 0xFF )
			{
				pid = pid & 0xFF;
				printf("ID mayor de 0xFF, usando byte menos significativo: 0x%X como ID\n", pid );
			}

        		frame.data[0+offset]   = 0x03;		/* 0x03 -> longitud 3 bytes, se manda pid & frame.data[3]=0x00 */
        		frame.data[2+offset]   = pid;
			break;

		case 0x22:	/* Read Data by Common Identifier */
		case 0x2F:	/* Input Output Control By Common Identifier */
        		frame.data[0+offset]   = 0x03;		/* 0x03 -> longitud 3 bytes */
			frame.data[2+offset]	= (pid & 0xFF00) >> 8;
			frame.data[3+offset]	= (pid & 0x00FF);
			break;

	
		case 0x04:	/* Clear Diagnostic Trouble Codes and stored values */
        		frame.data[0+offset]   = 0x01;		/* 0x01 -> longitud 1 byte, se manda servicio */
			printf("Esto borrará los codigos de diagnostico almacenados. Pendiente de implementar\n");
			exit(-1000); /* pte. implementar */

		case 0x11:	/* ECU Reset */
		case 0x14:	/* Clear Diagnostic Information */
		case 0x19:	/* Read DTC Information */
		case 0x23:	/* Read Memory by Address */
		case 0x27:	/* Security Access */
		case 0x28:	/* Communication Control */
		case 0x2A:	/* Read Data by Periodic ID */
		case 0x2C:	/* Dynamically Define Local Identifier */
		case 0x2E:	/* Write Data by Common Identifier */
		case 0x34:	/* Request Download */
		case 0x35:	/* Request Upload */
		case 0x36:	/* Transfer Data */
		case 0x37:	/* Request Transfer Exit */
		case 0x38:	/* Start Routine By Address */
		case 0x39:	/* Stop Routine By Address */
		case 0x3A:	/* Request Routine Results By Address */
		case 0x3B:	/* Write Data By Local Identifier */
		case 0x3D:	/* Write Memory by Address */
		case 0x85:	/* Control DTC Setting */
			printf("Servicio 0x%X pendiente de implementar\n", servicio);
			exit(-1000); /* pte. implementar */
			break;

		default: /* No aplica o reservados */
			printf("Servicio 0x%X: No aplica, reservado.\n", servicio);
			exit(-1001);
			break;

	}

	rc = can_escribe_trama( &frame );

	return rc;
}

/*
 * Funciones de tratamiendo de respuesta del mensaje remitido por la ECU
 *
 * Devuelve > 0 si hay que continuar escuchando
 *          = 0 si ha finalizado
 *          < 0 si ha fallado
 */

LOCAL int obd2_no_tratado( unsigned char * datos, int longitud )
{
	int i;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	char * strptr;

	if(  obd2_data[sv][pd].desc != NULL )
		strptr="";
	else
		strptr =  obd2_data[sv][pd].desc;

	printf("	OBD2 Servicio: 0x%X PID:0x%X (%s) : ", sv, pd, strptr );
	
	for( i = 2; i < longitud ; i++ )
		printf("%02X ", datos[i] );

	printf("\n\tOBD2 Servicio: 0x%X PID:0x%X (%s) : ", sv, pd, strptr );
	for( i = 2; i < longitud ; i++ )
		printf(" %c ", isprint( datos[i] ) ? datos[i] : '.' ) ;

	printf("\n\n");

	return 0;
}

LOCAL char * obten_dtc( unsigned int d1, unsigned int d2 )
{
	static char dtc[512];
	char area;
	int ia, val;

	switch ( d1 & 0xC0 )
	{
		case 0x00:
			area = 'P'; /* Powertrain */
			ia = DTC_P;
			break; 

		case 0x40:
			area = 'C'; /* Chasis */
			ia = DTC_C;
			break;

		case 0x80:
			area = 'B'; /* Body */
			ia = DTC_B;
			break;

		case 0xC0:
			area = 'U'; /* Network */
			ia = DTC_U;
			break;

		default:
			/* ¿¿?? */
			area = 'X';
			return "DTC NO VALIDO";

	}

	val = (d1 & 0x3F) * 256 +  d2; 
	sprintf( dtc, "%c%02X%02X: %s", area, d1 & 0x3F, d2, obd2_dtc_desc[ia][val].desc );

	return dtc;
}

LOCAL int obd2_dtc( unsigned char * datos, int longitud )
{
	int dx;
	char * dtc;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	dtc = obten_dtc( datos[dx], datos[dx+1] );

	printf(" %s\n", dtc );

	return 0;
}

LOCAL int obd2_obten_dtcs ( unsigned char * datos, int longitud )
{
	int sv = datos[0] & 0xBF;
	int pd = datos[1];
	int i = 0;

	/* if ( sv != 0x13 ) ...   */


	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	printf("Número de DTCs : %d\n", datos[1] );


	while( i < datos[1] )
	{
		printf("\tDTC #%d - %s\n", i+1, obten_dtc( datos[2+2*i], datos[2+2*i+1]) );
		i++;
	}

	return 0;
}


LOCAL int obd2_porcentaje( unsigned char * datos, int longitud )
{
	int dx;
        float f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = datos[dx] * 100.0 / 255.0;

	printf(" %f %s\n", f, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_maximum( unsigned char * datos, int longitud )
{
	int dx;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	printf("\n Máximo valor dosado ..................: %d\n", datos[dx] );
	printf("Máxima tensión del sensor de oxígeno .: %d (V)\n", datos[dx+1] );
	printf("Máxima corriente del sensor de oxígeno: %d (mA)\n", datos[dx+2] );
	printf("Máxima presión absoluta en la admisión: %d (kPa)\n", datos[dx+3] * 10 );

	return 0;
}

LOCAL int obd2_inout( unsigned char * datos, int longitud )
{
	int dx;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	printf("\n\n");

	if( ( datos[dx] & 0x01 ) != 0 )
	{
		printf("		Estado de la toma de fuerza ...............: Soportado - " );

		if( ( datos[dx+1] & 0x01 ) != 0 )
			printf( "ON\n" );
		else
			printf( "OFF\n");
	}


	if( ( datos[dx] & 0x02 ) != 0 )
	{
		printf("		Estado de la transmision automática .......: Soportado - " );

		if( ( datos[dx+1] & 0x02 ) != 0 )
			printf( "Marcha engranada\n");
		else
			printf( "Neutral\n" );
	}


	if( ( datos[dx] & 0x04 ) != 0 )
	{
		printf("		Estado de la transmisión manual ...........: Soportado - " );

		if( ( datos[dx+1] & 0x04 ) != 0 )
			printf( "Marcha engranada\n");
		else
			printf( "Punto muerto (o embrague pisado)\n" );
	}

	if( ( datos[dx] & 0x08 ) != 0 )
	{
		printf("		Estado de la luz indicadora de encendido ..: Soportado - " );

		if( ( datos[dx+1] & 0x08 ) != 0 )
			printf( "ON\n" );
		else
			printf( "OFF\n");
	}

	if( ( datos[dx] & 0xF0 ) != 0 )
		printf("		Usando un bit reservado D2=%02X\n", datos[dx] );

	if( ( datos[dx+1] & 0xF0 ) != 0 )
		printf("		Usando un bit reservado D3=%02X\n", datos[dx+1] );

	return 0;
}

LOCAL void  obd2_desc_fuel( unsigned char val )
{
	int i = 0;
	
	if( val & 0x01 )
	{
		printf("Bucle abierto debido a temperatura insuficiente en el motor\n" );
		i++;
	}

	if( val & 0x02 )
	{
		printf("Bucle cerrado, usando información del sensor de oxígeno para determinar el dosado\n" );
		i++;
	}

	if( val & 0x04 )
	{
		printf("Bucle abierto debido a la carga del motor o a un corte de combustible debido a la deceleración\n" );
		i++;
	}

	if( val & 0x08 )
	{
		printf("Bucle abierto debido a fallo del sistema\n" );
		i++;
	}

	if( val & 0x10 )
	{
		printf("Bucle cerrado, usando por lo menos un sensor de oxígeno, pero exite un fallo en el sistema\n" );
		i++;
	}

	if( i == 0 )
		printf("INFORMACION NO DISPONIBLE\n" );

	return;
}

LOCAL int obd2_sensores_ox( unsigned char * datos, int longitud )
{
	int dx;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	printf("\n\n" );

	if( datos[dx] & 0x01 )
		printf("		Banco 1 Sensor 1 presente\n" );
	
	if( datos[dx] & 0x02 )
		printf("		Banco 1 Sensor 2 presente\n" );
	
	if( datos[dx] & 0x04 )
		printf("		Banco 1 Sensor 3 presente\n" );
	
	if( datos[dx] & 0x08 )
		printf("		Banco 1 Sensor 4 presente\n" );
	
	if( datos[dx] & 0x10 )
		printf("		Banco 2 Sensor 1 presente\n" );
	
	if( datos[dx] & 0x20 )
		printf("		Banco 2 Sensor 2 presente\n" );
	
	if( datos[dx] & 0x40 )
		printf("		Banco 2 Sensor 3 presente\n" );
	
	if( datos[dx] & 0x80 )
		printf("		Banco 2 Sensor 4 presente\n" );
	
	return 0;
}


LOCAL int obd2_aire_2( unsigned char * datos, int longitud )
{
	int dx;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	printf("\n" );

	if( datos[dx] & 0x01 )
		printf("	Anterior al convertidor catalítico\n" );
	
	if( datos[dx] & 0x02 )
		printf("	Posterior al convertidor catalítico\n" );
	
	if( datos[dx] & 0x04 )
		printf("	Desde el exterior o desactivado\n" );
	
	if( datos[dx] & 0x08 )
		printf("	Bomba activada para diagnóstico\n" );
	
	return 0;
}

LOCAL int obd2_status_fuel( unsigned char * datos, int longitud )
{
	int dx;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	printf("\n\n		Sistema de combustible #1 .: " );
	obd2_desc_fuel( datos[dx] );
	
	printf("		Sistema de combustible #2 .: " );
	obd2_desc_fuel( datos[dx+1] );
	
	return 0;
}

LOCAL int obd2_mon_diag( unsigned char * datos, int longitud )
{
	int dx;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	if( pd == 0x01 )
	{
		if( datos[dx] & 0x80 )
			printf("\n\n		Indicador de mal funcionamiento (MIL) .......................: Encendido\n" );
		else
			printf("\n\n		Indicador de mal funcionamiento (MIL) .......................: Apagado\n" );

		printf("		Número de alarmas disponibles para su visualización .........: %d\n\n", datos[dx] & 0x3F );
	}
	else if( pd == 0x41 )
	{
		printf("\n\n" );
		if( datos[dx] != 0 )
			printf("		Incumplimiento de protocolo en byte D2: %02X\n", datos[dx] );

	}

	printf("		TESTS generales:\n");

	printf("			Monitorización de Componentes .......................: ");

	if( datos[dx+1] & 0x04 )
	{
		if( datos[dx+1] & 0x40 )
			printf("INCOMPLETA\n");
		else
			printf("COMPLETA\n");
	}
	else
		printf("NO DISPONIBLE\n");

	printf("			Monitorización del Sistema de Combustible ...........: ");
	if( datos[dx+1] & 0x02 )
	{
		if( datos[dx+1] & 0x20 )
			printf("INCOMPLETA\n");
		else
			printf("COMPLETA\n");
	}
	else
		printf("NO DISPONIBLE\n");

	printf("			Monitorización de Fallos ............................: ");
	if( datos[dx+1] & 0x01 )
	{
		if( datos[dx+1] & 0x10 )
			printf("INCOMPLETA\n");
		else
			printf("COMPLETA\n");
	}
	else
		printf("NO DISPONIBLE\n");

	printf("\n		TESTS específicos:\n");

	printf("			Monitorización del sistema EGR ......................: ");
	if( datos[dx+2] & 0x80 )
	{
		if( datos[dx+3] & 0x80 )
			printf("INCOMPLETA\n");
		else
			printf("COMPLETA\n");
	}
	else
		printf("NO DISPONIBLE\n");

	printf("			Monitorización del calentador de la sonda de oxígeno : ");
	if( datos[dx+2] & 0x40 )
	{
		if( datos[dx+3] & 0x40 )
			printf("INCOMPLETA\n");
		else
			printf("COMPLETA\n");
	}
	else
		printf("NO DISPONIBLE\n");


	printf("			Monitorización de la sonda de oxígeno ...............: ");
	if( datos[dx+2] & 0x20 )
	{
		if( datos[dx+3] & 0x20 )
			printf("INCOMPLETA\n");
		else
			printf("COMPLETA\n");
	}
	else
		printf("NO DISPONIBLE\n");


	printf("			Monitorización del refrigerante del sistema A/C .....: ");
	if( datos[dx+2] & 0x10 )
	{
		if( datos[dx+3] & 0x10 )
			printf("INCOMPLETA\n");
		else
			printf("COMPLETA\n");
	}
	else
		printf("NO DISPONIBLE\n");


	printf("			Monitorización del sistema secundario de aire .......: ");
	if( datos[dx+2] & 0x08 )
	{
		if( datos[dx+3] & 0x08 )
			printf("INCOMPLETA\n");
		else
			printf("COMPLETA\n");
	}
	else
		printf("NO DISPONIBLE\n");


	printf("			Monitorización del sistema de evaporación ...........: ");
	if( datos[dx+2] & 0x04 )
	{
		if( datos[dx+3] & 0x04 )
			printf("INCOMPLETA\n");
		else
			printf("COMPLETA\n");
	}
	else
		printf("NO DISPONIBLE\n");


	printf("			Monitorización del calentador del catalizador .......: ");
	if( datos[dx+2] & 0x02 )
	{
		if( datos[dx+3] & 0x02 )
			printf("INCOMPLETA\n");
		else
			printf("COMPLETA\n");
	}
	else
		printf("NO DISPONIBLE\n");


	printf("			Monitorización del catalizador ......................: ");

	if( datos[dx+2] & 0x01 )
	{
		if( datos[dx+3] & 0x01 )
			printf("INCOMPLETA\n");
		else
			printf("COMPLETA\n");
	}
	else
		printf("NO DISPONIBLE\n");

	return 0;
}

LOCAL int obd2_egr( unsigned char * datos, int longitud )
{
	int dx;
        float f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = ( datos[dx] * 100.0 / 128.0 ) - 100.0;

	printf(" %f %s\n", f, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_bancos_ox( unsigned char * datos, int longitud )
{
	int dx;
        float f, g;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = ( datos[dx] * 100.0 / 128.0 ) - 100.0;
	g = ( datos[dx+1] * 100.0 / 128.0 ) - 100.0;

	printf(" A: %f %s - B: %f %s\n", f, obd2_data[sv][pd].unidades, g, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_torque( unsigned char * datos, int longitud )
{
	int dx;
        int t;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	t = datos[dx] - 125;

	printf(" %d %s\n", t, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_temp( unsigned char * datos, int longitud )
{
	int dx;
        int t;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	t = datos[dx] - 40;

	printf(" %d %s\n", t, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_flujo_max( unsigned char * datos, int longitud )
{
	int dx;
        int t;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	t = datos[dx] * 10;

	printf(" %d %s\n", t, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_byte( unsigned char * datos, int longitud )
{
	int dx;
        int t;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	t = datos[dx];

	printf(" %d %s\n", t, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_long( unsigned char * datos, int longitud )
{
	int dx;
        int t;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	t = datos[dx] * 256 + datos[dx+1];

	printf(" %d %s\n", t, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_long10( unsigned char * datos, int longitud )
{
	int dx;
        int t;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	t = 10 * ( datos[dx] * 256 + datos[dx+1] );

	printf(" %d %s\n", t, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_triplebyte( unsigned char * datos, int longitud )
{
	int dx;
        int t;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	t = datos[dx] * 3;

	printf(" %d %s\n", t, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_fuel( unsigned char * datos, int longitud )
{
	int dx;
        float f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = 100.0 * datos[dx] / 128.0 - 100.0;

	printf(" %f %s\n", f, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_consumo( unsigned char * datos, int longitud )
{
	int dx;
        float f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = ( 256.0 * datos[dx] + datos[dx+1] ) / 20.0;

	printf(" %f %s\n", f, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_sync( unsigned char * datos, int longitud )
{
	int dx;
        float f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = ( 256.0 * datos[dx] + datos[dx+1] ) / 128.0 - 210.0;

	printf(" %f %s\n", f, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_abs_evap( unsigned char * datos, int longitud )
{
	int dx;
        float f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = ( 256.0 * datos[dx] + datos[dx+1] ) / 200.0 ;

	printf(" %f %s\n", f, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_evap( unsigned char * datos, int longitud )
{
	int dx;
        int f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = ( 256.0 * datos[dx] + datos[dx+1] ) - 32767 ;

	printf(" %d %s\n", f, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_rpm( unsigned char * datos, int longitud )
{
	int dx;
        float f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = ( 256.0 * datos[dx] + datos[dx+1] ) / 4.0 ;

	printf(" %f %s\n", f, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_avance( unsigned char * datos, int longitud )
{
	int dx;
        float f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = datos[dx] / 2.0 - 64.0 ;

	printf(" %f %s\n", f, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_maf( unsigned char * datos, int longitud )
{
	int dx;
        float f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = ( datos[dx] * 256.0 + datos[dx+1] ) / 100.0;

	printf(" %f %s\n", f, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_voltios( unsigned char * datos, int longitud )
{
	int dx;
        float f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = ( datos[dx] * 256.0 + datos[dx+1] ) / 1000.0;

	printf(" %f %s\n", f, obd2_data[sv][pd].unidades );

	return 0;
}
 
LOCAL int obd2_carga( unsigned char * datos, int longitud )
{
	int dx;
        float f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = 100.0 * ( datos[dx] * 256.0 + datos[dx+1] ) / 255.0;

	printf(" %f %s\n", f, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_dosado( unsigned char * datos, int longitud )
{
	int dx;
        float f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = 2.0 * ( datos[dx] * 256.0 + datos[dx+1] ) / 65536.0;

	printf(" %f %s\n", f, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_catalizador( unsigned char * datos, int longitud )
{
	int dx;
        float f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = ( datos[dx] * 256.0 + datos[dx+1] ) / 10.0 - 40;

	printf(" %f %s\n", f, obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_oxsensor_1( unsigned char * datos, int longitud )
{
	int dx;
        float f;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = datos[dx] / 200.0;

	printf(" %f %s", f, obd2_data[sv][pd].unidades );

	if( datos[dx+1] != 0xFF )
	{
		f = datos[dx+1] * 100.0 / 128.0 - 100.0;
		printf(". %f %%\n", f );
	}
	else
		printf("\n");

	return 0;
}

LOCAL int obd2_oxsensor_2( unsigned char * datos, int longitud )
{
	int dx;
        float f, v;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = 2 * ( 256.0 * datos[dx] + datos[dx+1] ) / 65536.0;

	v = 8 * ( 256.0 * datos[dx+2] + datos[dx+3] ) / 65536.0;

	printf(" %f %s (%f v)\n", f, obd2_data[sv][pd].unidades, v );

	return 0;
}

LOCAL int obd2_oxsensor_3( unsigned char * datos, int longitud )
{
	int dx;
        float f, a;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	f = 2 * ( 256.0 * datos[dx] + datos[dx+1] ) / 65536.0;

	a = datos[dx+2] + datos[dx+3] / 256.0 - 128.0;

	printf(" %f %s (%f mA)\n", f, obd2_data[sv][pd].unidades, a );

	return 0;
}

LOCAL int obd2_tipo_fuel( unsigned char * datos, int longitud )
{
	int dx;
        int tipo;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	tipo = datos[dx];

	if( tipo >= OBD_FUEL )
		tipo = 0;

	printf(" %s %s\n", obd2_tipo_combustible[tipo], obd2_data[sv][pd].unidades );

	return 0;
}

LOCAL int obd2_tipo( unsigned char * datos, int longitud )
{
	int dx;
        int tipo;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	dx = ( sv == 0x02 ) ? 3 : 2;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	tipo = datos[dx];

	if( tipo >= OBD_TYPES )
		tipo = OBD_TYPES-1;

	printf(" %s %s\n", obd2_tipo_obd[tipo], obd2_data[sv][pd].unidades );

	return 0;
}


LOCAL char * obd_s6_testid( int id )
{
	static char teststr[80];

	/*
	 * Segun SAE J1979 / ISO 15031-4
	 *
	 * Anexo C
	 *
	 * Definición de identificadores de Tests
	 *
	 */

	static char * obd2_test[0x0A+1] = {
		"Reservado",
		"Tensión de umbral de empobrecimiento de la mezcla (constante)",
		"Tensión de umbral de enriquecimiento de la mezcla (constante)",
		"Sensor de baja tensión para el calculo del tiempo de conmutación (constante)",
		"Sensor de alta tensión para el calculo del tiempo de conmutación (constante)",
		"Tiempo de conmuitación del sensor de empobrecimiento de mezcla (calcudado)",
		"Tiempo de conmuitación del sensor de enriquecimiento de mezcla (calcudado)",
		"Tensión mínima del sensor para el ciclo de prueba (calcudado)",
		"Tensión máxima del sensor para el ciclo de prueba (calcudado)",
		"Tiempo de conmutación de los sensores (calcudado)",
		"Periodo del sensor(calcudado)",
	};

	if( id <= 0x0A )
		sprintf( teststr, "Test ID ........: 0x%02X %s", id , obd2_test[id]);
	else if ( id > 0x0A && id < 0x20 )
		sprintf( teststr, "Test ID ........: 0x%02X Reservado ISO/SAE", id );
	else if ( id >= 0x20 )
		sprintf( teststr, "Test ID ........: 0x%02X Test definido por el fabricante", id );

	return teststr;
}

LOCAL char * obd_s6_unidades( int id )
{
	static char unitstr[30];

	sprintf( unitstr, "UASID ..........: 0x%02X %s (%s)", id, obd2_unidades[id&0x7F].desc, obd2_unidades[id&0x7F].unidades );

	return unitstr;
}

LOCAL char * obd_s6_valor( uint8_t id, uint8_t hi, uint8_t lo )
{
	static char valstr[90];
	uint16_t v;
	float valor;

	int indice = id & 0x7F;

	v = hi * 0x100 + lo;

	if( id & 0x80 )
	{
		int16_t vs;

		vs = (int16_t)v;

		valor = (float)vs * obd2_unidades[indice].escala;
	}
	else
		valor = (float)v * obd2_unidades[indice].escala + obd2_unidades[indice].offset;

	sprintf( valstr, "%f %s", valor, obd2_unidades[indice].unidades );

	return valstr;
}

LOCAL int obd2_s6( unsigned char * datos, int longitud )
{
	int sv = datos[0] & 0xBF;
	int pd = datos[1];
	int i;
	int tests;

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	printf("\n");

	if( ( longitud - 1 ) % OBD_S6_TEST_LEN != 0 )
		printf("	Longitud %d no esperada\n", longitud);

	tests =  ( longitud - 1 ) / OBD_S6_TEST_LEN ;

	for( i = 0; i < tests; i++ )
	{
		int indice;

		indice = 1 + i * OBD_S6_TEST_LEN;

		if( datos[indice] == pd )
			printf("	Test #%d:\n", i+1 );
		else
		{
			printf("Valor no esperado 0x%02X\n", datos[indice] );
		}

		printf("		%s\n", obd_s6_testid( datos[indice + 1] ) );

		if( datos[indice + 1] > 0x80 ) /* Test ID definido por el fabricante con unidades y escalas propias */
		{
			char * aux1;
			char * aux2;

			printf("		%s o definido por el fabricante (¿?)\n", obd_s6_unidades( datos[indice + 2] ) );

			aux2 =  obd_s6_valor( datos[indice + 2],  datos[indice + 3], datos[indice + 4] );
			aux1 = strdup( aux2 );
			aux2 = obd_s6_valor( 0x01,  datos[indice + 3], datos[indice + 4] ); /* Datos en bruto */
			printf("		Valor ..........: %s (%s)\n", aux1, aux2 );
			free( aux1 );

			aux2 =  obd_s6_valor( datos[indice + 2],  datos[indice + 5], datos[indice + 6] );
			aux1 = strdup( aux2 );
			aux2 = obd_s6_valor( 0x01,  datos[indice + 5], datos[indice + 6] ); /* Datos en bruto */
			printf("		Límite inferior : %s (%s)\n", aux1, aux2 );
			free( aux1 );

			aux2 =  obd_s6_valor( datos[indice + 2],  datos[indice + 7], datos[indice + 8] );
			aux1 = strdup( aux2 );
			aux2 = obd_s6_valor( 0x01,  datos[indice + 7], datos[indice + 8] ); /* Datos en bruto */
			printf("		Límite superior : %s (%s)\n", aux1, aux2 );
			free( aux1 );
		}
		else
		{
			printf("		%s\n", obd_s6_unidades( datos[indice + 2] ) );
			printf("		Valor ..........: %s\n", obd_s6_valor( datos[indice + 2],  datos[indice + 3], datos[indice + 4] ) );
			printf("		Límite Inferior : %s\n", obd_s6_valor( datos[indice + 2],  datos[indice + 5], datos[indice + 6] ) );
			printf("		Límite Superior : %s\n\n", obd_s6_valor( datos[indice + 2],  datos[indice + 7], datos[indice + 8] ) );
		}
	}


	return 0;
}

LOCAL int obd2_s9_vin( unsigned char * datos, int longitud )
{
        int i;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	for( i = 2; i < longitud; i++ )
		if( datos[i] == 0x00 )
			datos[i] = ' '; /* convierte los 0x00 en blancos */
	
	printf("%s\n", &datos[3] );

	return 0;
}

LOCAL int obd2_s9_cvn( unsigned char * datos, int longitud )
{
        int i;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	for( i = 2; i < longitud; i++ )
		printf("%02X", datos[i] );

	printf("\n");

	return 0;
}

LOCAL int obd2_s9_tracking( unsigned char * datos, int longitud )
{
        int i, contador;
	int sv = datos[0] & 0xBF;
	int pd = datos[1];

	char * texto[20] = {
		"	OBDCOND   - Condiciones de Monitorización OBD encontradas ...............................................................:",
		"	IGNCNTR   - Número de veces que se ha arrancado el motor ................................................................:",
		"	CATCOMP1  - N.º de veces con las condiciones necesarias para detectar un fallo del sistema de catalizador del banco 1 ...:",
		"	CATCOND1  - N.º de usos del vehículo en las condiciones especificadas de control del catalizador del banco 1 ............:",
		"	CATCOMP2  - N.º veces con las condiciones necesarias para detectar un fallo del sistema de catalizador del banco 2 ......:",
		"	CATCOND2  - N.º de usos del vehículo en las condiciones especificadas de control del catalizador del banco 2 ............:",
		"	O2SCOMP1  - N.º veces con las condiciones necesarias para detectar un fallo del sensor de oxígeno del banco 1 ...........:",
		"	O2SCOND1  - N.º de usos del vehículo en las condiciones especificadas de control del sensor de oxígeno del banco 1 ......:",
		"	O2SCOMP2  - N.º veces con las condiciones necesarias para detectar un fallo del sensor de oxígeno del banco 2 ...........:",
		"	O2SCOND2  - N.º de usos del vehículo en las condiciones especificadas de control del sensor de oxígeno del banco 2 ......:",
		"	EGRCOMP   - N.º veces con las condiciones necesarias para detectar un fallo en el sistema EGR/VVT .......................:",
		"	EGRCOND   - N.º de usos del vehículo en las condiciones especificadas de control del sistema EGR/VVT ....................:",
		"	AIRCOMP   - N.º veces con las condiciones necesarias para detectar un fallo en el sistema secundario de aire ............:",
		"	AIRCOND   - N.º de usos del vehículo en las condiciones especificadas de control del sistema secundario de aire .........:",
		"	EVAPCOMP  - N.º veces con las condiciones necesarias para detectar un fallo en el sistema de monitorización EVAP ........:",
		"	EVAPCOND  - N.º de usos del vehículo en las condiciones especificadas de control del sistema de monitorización EVAP .....:",
		"	SO2SCOMP1 - N.º veces con las condiciones necesarias para detectar un fallo del sensor de oxígeno secundario del banco 1 :",
		"	SO2COND1  - N.º de usos del vehículo en las condiciones especificadas de control del sensor de oxígeno del banco 1 ......:",
		"	SO2SCOMP2 - N.º veces con las condiciones necesarias para detectar un fallo del sensor de oxígeno secundario del banco 2 :",
		"	SO2COND2  - N.º de usos del vehículo en las condiciones especificadas de control del sensor de oxígeno del banco 2 ......:"
	};
		

	if(  obd2_data[sv][pd].desc != NULL )
		printf("	%s : ", obd2_data[sv][pd].desc );
	else
		printf("NULL : " );

	printf("\n");
	
	/*
	for( i = 0; i < longitud; i=i+2 )
		printf("0x%02X%02X\n", datos[i+2], datos[i+3] );
	*/

	contador = datos[2];

	if( (contador * 2 + 3) > longitud )
		printf("Error: longitud insuficiente ... ¿¿??");

	printf("\n   Recibidos %d contadores:\n\n", contador );

	for( i = 0; i < contador; i++ )
	{
		int val;
		printf("%s ", texto[i]);

		val = datos[3+i*2] * 256 + datos[3+i*2+1] ;

		if( val == 0 )
			printf( "NO SOPORTADO\n");
		else 
			printf("%d\n", val );
	}

	return 0;
}

LOCAL int obd2_pids_soportados( unsigned char * datos, int longitud )
{
	int dx;
	int j;
	int rc = 0;

	int servicio = datos[0] -0x40;
	int local_pid = datos[1];			/* datos[1] -> PID */

	dx = ( servicio == 0x02 ) ? 3 : 2;

	for( j = 0; j < longitud-2; j++ )
	{
		int i;
		int flag=0x80;

		for( i = 1; i < 9; i++ )
		{
			int aux;

			if( datos[dx+j] & flag )
			{
				aux = local_pid + (j*0x08) + i;

				printf("	PID 0x%02X: ", aux); 
				obd2_data[servicio][aux].soportado = 1; 

				if(  obd2_data[servicio][aux].desc != NULL )
				{
					char * caux;
					char * tok;

					caux = strdup( obd2_data[servicio][aux].desc );

					tok = caux;
					while( *tok != '\0' )
						if( *tok == '.' )
							*tok='\0';
						else
							tok++;

					printf("%s\n", caux );

					free( caux );
				}
			}

			flag= flag / 2;
		}
	}

	printf("-->> PID 0x%X flag: %d -> %s \n", local_pid+0x20, obd2_data[servicio][local_pid+0x20].soportado, obd2_data[servicio][local_pid+0x20].desc );

	if( obd2_data[servicio][local_pid+0x20].soportado == 1 )
	{
		pid = local_pid + 0x20;
		obd2_data[servicio][local_pid+0x20].soportado = 2;	 /* Para evitar que se vuelva a solicitar en la fase de consulta */

		rc = obd2_solicita_servicio_pid( servicio );
	}

	return rc;
}


/*
 * **************
 */

PUBLIC void obd2_inicializa_funciones( void )
{
	int i, j;

	for( i = 0; i < OBD_SERVICES; i++ )
		for( j = 0; j < OBD_PIDS; j++ )
		{
			obd2_data[i][j].manejador_obd2 = &obd2_no_tratado;	
			obd2_data[i][j].soportado = 0;
			obd2_data[i][j].len = -1;
			obd2_data[i][j].desc = "\n";
			obd2_data[i][j].unidades = "";
		}

	obd2_data[0x01][0x00].soportado = 1; /* Por norma */
	obd2_data[0x02][0x00].soportado = 1; /* Por norma */
	obd2_data[0x05][0x00].soportado = 1; /* Por norma */
	obd2_data[0x06][0x00].soportado = 1; /* Por norma */
	obd2_data[0x08][0x00].soportado = 1; /* Por norma */
	obd2_data[0x09][0x00].soportado = 1; /* Por norma */
	obd2_data[0x21][0x00].soportado = 1; /* Por norma */

	#include "obd2_serv1.h" 	/* también incluye las definiciones para el servicio 0x02 */
	#include "obd2_serv5.h"
	#include "obd2_serv6.h"
	#include "obd2_serv8.h"
	#include "obd2_serv9.h"
	#include "obd2_serv13.h"
	#include "obd2_serv21.h"

	for( i = 0; i < OBD_UNIDADES; i++ )
	{
		obd2_unidades[i].desc = "\n";
		obd2_unidades[i].unidades = "";
		obd2_unidades[i].escala = 1.0;
		obd2_unidades[i].offset = 0.0;
	}

	#include "obd2_unidades.h"


	for( i = 0; i < OBD2_DTC_TYPES; i++ )
		for( j = 0; j < OBD2_MAX_DTCS; j++ )
		{
			obd2_dtc_desc[i][j].desc = "Undefined";
		}

	#include "dtc_bcodes.h"
	#include "dtc_ccodes.h"
      	#include "dtc_pcodes.h"
	#include "dtc_ucodes.h"


	for( i = 0; i< 8; i++ )
	{
		obd2_trama[i].def = 0;
		obd2_trama[i].dato = 0;
	}

	return;
}


LOCAL int obd2_procesa_trama( int servicio, unsigned char * datos, int longitud )
{
	int rc;

	if( 0x7F == datos[0] )
	{
		printf("Respuesta negativa a servicio 0x%X: %s\n", servicio, get_respuesta_14229( datos[2] ) );
		return -3;
	}

	if( (0x40 | servicio) != datos[0] )
	{
		printf("Servicio: %X - %X\n", (0x40 | servicio), datos[0] );
		return -1;
	}

	if( servicio == 0x13 || servicio == 0x1A || servicio == 0x10 || servicio == 0xA3 )
	{
		/* No hay validación de PID para estos servicios */
	}
	else
	{
		if( pid != datos[1] )
		{
			printf("PID: 0x%02X no esperado. Esperado: 0x%02X\n", datos[1], pid );
			return -2;
		}
	}

	if( obd2_data[servicio][pid].len > 0 )
		if( obd2_data[servicio][pid].len != longitud -2 )
			printf("	Longitud (%d) no esperada para servicio 0x%02X pid 0x%02X. Esperada %d\n", longitud - 2, servicio, pid, obd2_data[servicio][pid].len );


	rc = obd2_data[servicio][pid].manejador_obd2( datos, longitud );

	if( rc < 0 )
		printf("fallo en el tratamiento de trama %X %X\n", servicio, pid);

	return rc;
}

LOCAL int obd2_escribe_trama( int servicio, unsigned char * datos, int longitud, struct timespec *time_p )
{
	int i;

	if( 0x7F == datos[0] )
	{
		printf("Respuesta negativa a servicio 0x%X: %s\n", servicio, get_respuesta_14229( datos[2] ) );
		return -3;
	}

	if( (0x40 | servicio) != datos[0] )
	{
		printf("Servicio: %X - %X\n", (0x40 | servicio), datos[0] );
		return -1;
	}

	if( pid != datos[1] )
	{
		printf("PID ....: %X - %X\n", pid, datos[1] );
		return -2;
	}
	
	printf("(%ld.%06ld) ECU: 0x%03X Serv.: 0x%02X PID:0x%02X | ", time_p->tv_sec, time_p->tv_nsec/1000, get_ecu_address(), servicio, pid );

	for( i = 2; i < longitud ; i++ )
		printf("%02X ", datos[i] );

	printf(" | ");

	for( i = 2; i < longitud ; i++ )
		printf("%c ", isprint( datos[i] ) ? datos[i] : '.' );

	printf("\n");

	return 0;
}

LOCAL int obd2_control_flujo( int modo )
{
	int rc;
	struct can_frame frame;

	frame.can_id    = get_ecu_address();
       	frame.can_dlc   = 8;

	if( ext_address == NO_EXT_ADDRESS )
	{
		frame.data[0]   = CAN_FC | modo;
       		frame.data[1]   = 0x00;
	}
	else
	{
		frame.data[0]   = ext_address;
		frame.data[1]   = CAN_FC | modo;
	}

       	frame.data[2]   = 0x00;
       	frame.data[3]   = 0x01;	/* Separación mínima de 1 ms */
       	frame.data[4]   = 0x00;
       	frame.data[5]   = 0x00;
       	frame.data[6]   = 0x00;
       	frame.data[7]   = 0x00;

        rc = can_escribe_trama( &frame );

        if (rc != sizeof(struct can_frame) )
                return -1;

	return rc;
}
 
LOCAL int obd2_lee_trama( unsigned char * buffer, int secuencia )
{
	int rc;
	struct can_frame rframe;
	int offset;

	if( ext_address == NO_EXT_ADDRESS )
		offset = 0;
	else
		offset = 1;
        /*
	 *  Leemos una trama CAN
	 */
        rc = can_lee_trama( &rframe );

	if( rc == CAN_MTU )
	{
		int longitud;
		int long_aux;;
		int i;
		int bufferptr;

		if( get_ecu_address() == ECU_BROADCAST )
			set_ecu_address( rframe.can_id - 0x08 );

		if( rframe.can_id != get_ecu_address() + 0x08 )
		{
			if( can_get_verbose() > 0 )
				printf("Recibida trama de ECU 0x%X no esperada\n", rframe.can_id);
		}

		if( offset == 1)
		{
			if( rframe.data[0] != ext_address )
			{
				if( can_get_verbose() > 0 )
					printf("Ignorando trama de dirección extendida 0x%X no esperada. ECU: 0x%X\n", rframe.data[0], rframe.can_id);

				return obd2_lee_trama( buffer, secuencia);
			}
		}

		switch (rframe.data[0+offset] & 0xF0 )
		{
			
			case CAN_SF:	/* Single Frame */

				if( secuencia != 0 )
				{
					printf("Error de protocolo: secuencia %d no esperada. Esperada 0 (SF)\n", secuencia );
					return -1;
				}

				longitud = rframe.data[0+offset] & 0x0F;

				for( i = 0; i < longitud; i++ )
					buffer[i] = rframe.data[i+1+offset];

				buffer[i] = 0x00;

				return longitud;

			case CAN_FF:	/* First Frame */

				longitud = (rframe.data[0+offset] & 0x0F) * 256 + rframe.data[1+offset];
				long_aux = longitud;

				if( secuencia == 0 )
				{
					for( i = 0; i < 6-offset; i++ )
						buffer[i] = rframe.data[i+2+offset];

					bufferptr = 6-offset;
					long_aux = long_aux - 6+offset;

					rc = obd2_control_flujo( CAN_FC_CONTINUE );

					if( rc < 0 )
					{
						puts("Error enviando control de flujo");
						return -1;
					}
				}
				else
				{
					printf("Error de protocolo: secuencia %d no esperada. Esperada 0 (FF)\n", secuencia );
					return -1;
				}

				while( long_aux > 0 )
				{
					secuencia++;

					if( secuencia > 0x0F )
						secuencia = 0x00;

					rc = obd2_lee_trama( &buffer[bufferptr], secuencia );

					if( rc < 0 )
					{
						puts("Error leyendo trama CF");
						return -1;
					}

 					long_aux = long_aux - 7 + offset; /* OJO ... se está suponiendo que hemos leido una trama CF */
					bufferptr = bufferptr + 7 + offset;
				}
					
				return longitud;

			case CAN_CF:	/* Continue Frame */

				if( secuencia != (rframe.data[0+offset] & 0x0F ) )
				{
					printf("Error de protocolo: secuencia %d no esperada. Esperada %d (CF)\n", (rframe.data[0+offset] & 0x0F ), secuencia );
					return -1;
				}

				for( i = 0; i < 7-offset; i++ )
					buffer[i] = rframe.data[i+1+offset];

				return 7-offset;

			case CAN_FC:	/* Flow Control Frame */

				return 0;

			default:
				puts("Error enviando control de flujo");
				return -1;
		}
	}

	puts("Error de protocolo");
	return -1;
}

PUBLIC int obd2_scan( int servicio_obd )
{
	int rc, i;
	unsigned char buffer[4352];	/* 16 * 256 + 255 + 1 */

	for( pid = 0x01; pid < OBD_PIDS; pid++ )
	{
		if( obd2_data[servicio_obd][pid].soportado == 1 )
		{
	        	rc = obd2_solicita_servicio_pid( servicio_obd );

        		if (rc != sizeof(struct can_frame) )
        		{
                		puts("Error al escribir en el bus");
                		rc = can_cierra_socket();
                		return -1;
        		}

        		do
        		{
                		rc = obd2_lee_trama( buffer, 0 );
		
                		if( rc < 0 )
                		{
                        		puts("Error leyendo trama");
                        		return -1;
                		}

				if( can_get_verbose() > 0 )
				{
                			printf("Recibidos %d datos: ", rc );
                			for( i = 0; i < rc; i++ )
                        			printf("%X ", buffer[i] );

                			printf("\n");
				}

                		rc = obd2_procesa_trama( servicio_obd, buffer, rc );
        		} while ( rc > 0 );
		}
	}

	return 0;
}

PUBLIC int obd2_start( int servicio_obd )
{
	int rc;
	int i;
	unsigned char buffer[4352];	/* 16 * 256 + 255 + 1 */

	printf( "\nOBD2 Servicio 0x%02x\n", servicio_obd );

	pid = get_default_param();

	rc = obd2_solicita_servicio_pid( servicio_obd );
		       
	if (rc != sizeof(struct can_frame) )
	{
		puts("Error al escribir en el bus");
        	rc = can_cierra_socket();
		return -1;
	}

	do
	{
		rc = obd2_lee_trama( buffer, 0 );

		if( rc < 0 )
		{
			puts("Error leyendo trama");
			return -1;
		}

		if( can_get_verbose() > 0 )
		{
			printf("Recibidos %d datos: ", rc );
			for( i = 0; i < rc; i++ )
				printf("%X ", buffer[i] );

			printf("\n");
		}

		rc = obd2_procesa_trama( servicio_obd, buffer, rc );
	} while ( rc > 0 );

	rc = obd2_scan( servicio_obd );

	/*
	for( pid = 0x01; pid < OBD_PIDS; pid++ )
	{
		if( obd2_data[servicio_obd][pid].soportado == 1 )
		{
	        	rc = obd2_solicita_servicio_pid( servicio_obd );

        		if (rc != sizeof(struct can_frame) )
        		{
                		puts("Error al escribir en el bus");
                		rc = can_cierra_socket();
                		return -1;
        		}

        		do
        		{
                		rc = obd2_lee_trama( buffer, 0 );
		
                		if( rc < 0 )
                		{
                        		puts("Error leyendo trama");
                        		return -1;
                		}

				if( can_get_verbose() > 0 )
				{
                			printf("Recibidos %d datos: ", rc );
                			for( i = 0; i < rc; i++ )
                        			printf("%X ", buffer[i] );

                			printf("\n");
				}

                		rc = obd2_procesa_trama( servicio_obd, buffer, rc );
        		} while ( rc > 0 );
		}
	}
	*/

        return rc;
}

LOCAL int64_t timespecDiff(struct timespec *timeA_p, struct timespec *timeB_p)
{
  return ((timeA_p->tv_sec * 1000000000) + timeA_p->tv_nsec) - ((timeB_p->tv_sec * 1000000000) + timeB_p->tv_nsec);
}


PUBLIC int obd2_pid( int servicio_obd, int frecuencia )
{
	int rc;
	int i;
	unsigned char buffer[256];

	long int usec;
	struct timespec inicio, fin;

	pid = get_default_param();

	while ( 1 )
	{
         	clock_gettime(CLOCK_REALTIME, &inicio);

		rc = obd2_solicita_servicio_pid( servicio_obd );
		       
		if (rc != sizeof(struct can_frame) )
		{
			puts("Error al escribir en el bus");
        		rc = can_cierra_socket();
			return -1;
		}

		do
		{
			rc = obd2_lee_trama( buffer, 0 );

			if( rc < 0 )
			{
				puts("Error leyendo trama");
				return -1;
			}

			if( can_get_verbose() > 0 )
			{
				printf("Recibidos %d datos: ", rc );
				for( i = 0; i < rc; i++ )
					printf("%X ", buffer[i] );
	
				printf("\n");
			}

			rc = obd2_escribe_trama( servicio_obd, buffer, rc, &inicio );
		} while ( rc > 0 );

         	clock_gettime(CLOCK_REALTIME, &fin);

		usec = 1000000 / frecuencia - timespecDiff( &fin, &inicio ) / 1000;

		if( usec > 0L )
			usleep( (useconds_t) usec );
	}

	/* NOTREACHED */
}



PUBLIC void obd2_set_dato(unsigned int i, unsigned int d )
{
	obd2_trama[i].def = 1;
	obd2_trama[i].dato = d;

	return;
}

PUBLIC char * obd2_get_dato( int i )
{
	static char str[64];

	if( obd2_trama[i].def == 1 )
	{
		sprintf( str, "0x%02X (%03d)", obd2_trama[i].dato, obd2_trama[i].dato );
		return str;
	}
	else
		return "No definido";	
}

PUBLIC void obd2_unset_dato( unsigned int d )
{
	obd2_trama[d].def = 0;

	return;
}

PUBLIC void obd2_set_long( unsigned int l )
{
	tr_long = l;
}

PUBLIC void obd2_set_serv( unsigned int s )
{
	tr_serv = s;
}

PUBLIC void obd2_set_pid( unsigned int p )
{
	tr_pid = p;
	pid = p;
	set_default_param(p);
}

PUBLIC unsigned int obd2_get_long ( void )
{
	return tr_long;
}

PUBLIC unsigned int obd2_get_serv ( void )
{
	return tr_serv;
}

PUBLIC unsigned int obd2_get_pid ( void )
{
	return tr_pid;
}

PUBLIC int obd2_send_buffer( unsigned int len_buffer, unsigned char * buffer, int replies )
{
	struct can_frame frame, rframe;
	unsigned char obd2_buffer[4095];
	int i, j, rc;
	unsigned int pos_buff = 0;
	unsigned int secuencia = 0;
	unsigned int bs_sec = 0;
	unsigned int offset;

	unsigned int fs, bs, stmin;
	useconds_t delay_micro;

	/*
	 * Configuramos la trama CAN a enviar ...
	 */
	frame.can_id    = get_ecu_address();
	frame.can_dlc   = 8;

	if( ext_address == NO_EXT_ADDRESS )
		offset = 0;
	else
	{
		offset = 1;
		frame.data[0]	= ext_address;
	}

	if( len_buffer > 0xFFF)
		return -1;

	if( len_buffer < 8 - offset )
		return -2; /* usar obd2_send */

        frame.data[offset] = 0x10 | ( ( len_buffer & 0xF00 ) >> 8 );
        frame.data[offset+1] = len_buffer & 0xFF;

	obd2_set_serv( buffer[pos_buff] );

	for( i = offset + 2; i < frame.can_dlc; i++ )
	{
        	frame.data[i] = buffer[pos_buff];
		pos_buff++;
		len_buffer--;
	}

	rc = can_escribe_trama( &frame );

	if (rc != sizeof(struct can_frame) )
	{
		puts("Error al escribir en el bus");
        	rc = can_cierra_socket();
		return -1;
	}

	secuencia++;

       	rc = can_lee_trama( &rframe );

	if( rc == CAN_MTU )
	{
		if( get_ecu_address() == ECU_BROADCAST )
			set_ecu_address( rframe.can_id - 0x08 );

		if( rframe.can_id != get_ecu_address() + 0x08 )
		{
			if( can_get_verbose() > 0 )
				printf("Recibida trama de ECU 0x%X no esperada\n", rframe.can_id);

			return -6;
		}

		if( offset == 1)
		{
			if( rframe.data[0] != ext_address )
			{
				if( can_get_verbose() > 0 )
					printf("Recibida trama de dirección extendida 0x%X no esperada. ECU: 0x%X\n", rframe.data[0], rframe.can_id);
	
				return -5;
			}
		}

		switch (rframe.data[0+offset] & 0xF0 )
		{
			case CAN_FC:	/* Flow Control Frame */
	
				fs = rframe.data[1+offset];
				bs = rframe.data[2+offset];
				stmin = rframe.data[3+offset];
				break;

			default:

				if( can_get_verbose() > 0 )
					printf("Recibida trama (%d) no esperada\n", rframe.data[0+offset] & 0xF0 );

				return -4;
		}

	}
	else
		return -3;

	if( fs == 2 ) /* OVFLOW */
	{
		if( can_get_verbose() > 0 )
			printf("Recibido OverFlow en control de flujo\n" );

		return -7;
	}
	else if ( fs > 2 ) /* FS Invalido */
	{
		if( can_get_verbose() > 0 )
			printf("Recibido FS inválido en control de flujo\n" );

		return -7;
	}

	/* else FS CTS -> Continue to Send */

	/* Obtenemos el retraso entre tramas en microsengudos */

	if( stmin < 0x80 )
	       delay_micro = stmin * 1000;
	else if ( stmin >= 0xF1 && stmin <= 0xF9 )
		delay_micro = (stmin & 0x0F ) * 100;
	else
		stmin = 0x7F * 1000;

	while( len_buffer > 0 )
	{
		frame.data[offset] = 0x20 | secuencia;
		
		if( len_buffer >= (unsigned int)(frame.can_dlc - 1) )
			for( i = offset + 1; i < frame.can_dlc; i++ )
			{
       				frame.data[i] = buffer[pos_buff];
				pos_buff++;
				len_buffer--;
			}
		else
		{
			i = offset + 1;

			while( len_buffer > 0 )
			{
       				frame.data[i++] = buffer[pos_buff++];
				len_buffer--;
			}

			for( ; i < frame.can_dlc; i++ )
       				frame.data[i] = 0x55;
		}
		
		usleep( delay_micro );

		rc = can_escribe_trama( &frame );

		if (rc != sizeof(struct can_frame) )
		{
			puts("Error al escribir en el bus");
       			rc = can_cierra_socket();
			return -7;
		}

		secuencia++;

		if( secuencia > 0x0F )
			secuencia = 0;

		if( bs > 0 )
		{
			bs_sec++;

			if( bs_sec > bs )
			{
       				rc = can_lee_trama( &rframe );

				if( rc == CAN_MTU )
				{
					if( get_ecu_address() == ECU_BROADCAST )
						set_ecu_address( rframe.can_id - 0x08 );

					if( rframe.can_id != get_ecu_address() + 0x08 )
					{
						if( can_get_verbose() > 0 )
							printf("Recibida trama de ECU 0x%X no esperada\n", rframe.can_id);
			
						return -6;
					}

					if( offset == 1)
					{
						if( rframe.data[0] != ext_address )
						{
							if( can_get_verbose() > 0 )
								printf("Recibida trama de dirección extendida 0x%X no esperada. ECU: 0x%X\n", rframe.data[0], rframe.can_id);
				
							return -5;
						}
					}

					if( (rframe.data[0+offset] & 0xF0 ) != CAN_FC )
					{
						if( can_get_verbose() > 0 )
						printf("Recibida trama (%d) no esperada\n", rframe.data[0+offset] & 0xF0 );

						return -4;
					}
				}
				else
					return -3;

				bs_sec = 0;
			}
		}

	}


	for( j = 0; j < replies; j++ )
		do
		{
			rc = obd2_lee_trama( obd2_buffer, 0 );

			if( rc < 0 )
			{
				puts("Error leyendo trama");
				return -1;
			}

			if( can_get_verbose() > 0 )
			{
				printf("Recibidos %d datos: ", rc );
				for( i = 0; i < rc; i++ )
					printf("%X ", buffer[i] );

				printf("\n");
			}

			rc = obd2_procesa_trama( tr_serv, obd2_buffer, rc );
		} while ( rc > 0 );


	return 0;
}

PUBLIC int obd2_send( int replies )
{
	int i,j;
	unsigned char buffer[4095];
	struct can_frame frame;
	int offset;
	int rc;

	pid = get_default_param();

	/*
	 * Configuramos la trama CAN a enviar ...
	 */
	frame.can_id    = get_ecu_address();
	frame.can_dlc   = 8;

	if( ext_address == NO_EXT_ADDRESS )
		offset = 0;
	else
	{
		offset = 1;
		frame.data[0]	= ext_address;
	}

        frame.data[offset] = tr_long;
        frame.data[offset+1] = tr_serv;
        frame.data[offset+2] = tr_pid;

	for( i = 0; i < 8; i++ )
		if( obd2_trama[i].def == 1)
        		frame.data[i]   = obd2_trama[i].dato;

	rc = can_escribe_trama( &frame );

	if (rc != sizeof(struct can_frame) )
	{
		puts("Error al escribir en el bus");
        	rc = can_cierra_socket();
		return -1;
	}

	for( j = 0; j < replies; j++ )
		do
		{
			rc = obd2_lee_trama( buffer, 0 );

			if( rc < 0 )
			{
				puts("Error leyendo trama");
				return -1;
			}

			if( can_get_verbose() > 0 )
			{
				printf("Recibidos %d datos: ", rc );
				for( i = 0; i < rc; i++ )
					printf("%X ", buffer[i] );

				printf("\n");
			}

			rc = obd2_procesa_trama( tr_serv, buffer, rc );
		} while ( rc > 0 );


	return 0;
}
