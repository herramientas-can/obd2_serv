/*
    Copyright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
   
    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */

#ifndef MP_OBD2_UNIDADES_H
#define MP_OBD2_UNIDADES_H

#ifndef MP_OBD2_H
include "obd2.h"
#endif 	/* MP_OBD2_H */

/*
 *
 * Según SAE J1979 (2006)
 * Anexo E
 * Definición de unidades y escalas para el servicio 0x06
 *
 */
DEFU( 0x01, "Valor bruto", "", 1.0, 0.0 )
DEFU( 0x02, "Valor bruto", "", 0.1, 0.0 )
DEFU( 0x03, "Valor bruto", "", 0.01, 0.0 )
DEFU( 0x04, "Valor bruto", "", 0.001, 0.0 )
DEFU( 0x05, "Valor bruto", "", 0.0000305, 0.0 )
DEFU( 0x06, "Valor bruto", "", 0.000305, 0.0 )
DEFU( 0x07, "Frecuencia de giro", "rpm", 0.25, 0.0 )
DEFU( 0x08, "Velocidad", "km/h", 0.01, 0.0 )
DEFU( 0x09, "Velocidad", "km/h", 1.0, 0.0 )
DEFU( 0x0A, "Tensión", "V", 0.122, 0.0 )
DEFU( 0x0B, "Tensión", "V", 0.001, 0.0 )
DEFU( 0x0C, "Tensión", "V", 0.01, 0.0 )
DEFU( 0x0D, "Intensidad", "A", 0.00390625, 0.0 )
DEFU( 0x0E, "Intensidad", "A", 0.001, 0.0 )
DEFU( 0x0F, "Intensidad", "A", 0.01, 0.0 )
DEFU( 0x10, "Tiempo", "s", 0.001, 0.0 )
DEFU( 0x11, "Tiempo", "s", 0.01, 0.0 )
DEFU( 0x12, "Tiempo", "s", 1.0, 0.0 )
DEFU( 0x13, "Resistencia", "Ohm", 0.001, 0.0 )
DEFU( 0x14, "Resistencia", "Ohm", 1.0, 0.0 )
DEFU( 0x15, "Resistencia", "Ohm", 1000.0, 0.0 )
DEFU( 0x16, "Temperatura", "ºC", 0.1, -40.0 )
DEFU( 0x17, "Presión", "kPa", 0.01, 0.0 )
DEFU( 0x18, "Presión", "kPa", 0.0117, 0.0 )
DEFU( 0x19, "Presión", "kPa", 0.079, 0.0 )
DEFU( 0x1A, "Presión", "kPa", 1.0, 0.0 )
DEFU( 0x1B, "Presión", "kPa", 10.0, 0.0 )
DEFU( 0x1C, "Ángulo", "º", 0.01, 0.0 )
DEFU( 0x1D, "Ángulo", "º", 0.5, 0.0 )
DEFU( 0x1E, "Ratio (lambda)", "", 0.0000305, 0.0 )
DEFU( 0x1F, "A/F Ratio", "", 0.05, 0.0 )
DEFU( 0x20, "Ratio", "", 0.0039062, 0.0 )
DEFU( 0x21, "Frecuencia", "Hz", 0.001, 0.0 )
DEFU( 0x22, "Frecuencia", "Hz", 1.0, 0.0 )
DEFU( 0x23, "Frecuencia", "Hz", 1000.0, 0.0 )
DEFU( 0x24, "Veces", "veces", 1.0, 0.0 )
DEFU( 0x25, "Distancia", "km", 1.0, 0.0 )
DEFU( 0x26, "Tensión/tiempo", "V/ms", 0.1, 0.0 )
DEFU( 0x27, "Masa/tiempo", "g/s", 0.01, 0.0 )
DEFU( 0x28, "Masa/tiempo", "g/s", 1.0, 0.0 )
DEFU( 0x29, "Presión/tiempo", "Pa/s", 0.25, 0.0 )
DEFU( 0x2A, "Masa/tiempo", "g/s", 0.001, 0.0 )
DEFU( 0x2B, "Conmutaciones", "conmutaciones", 1.0, 0.0 )
DEFU( 0x2C, "Masa/cilindro", "g/cil", 0.01, 0.0 )
DEFU( 0x2D, "Masa/carrera", "g/carrera", 0.01, 0.0 )
DEFU( 0x2E, "Booleano", "", 1.0, 0.0 )
DEFU( 0x2F, "Porcentaje", "%", 0.01, 0.0 )
DEFU( 0x30, "Porcentaje", "%", 0.001526, 0.0 )
DEFU( 0x31, "Volumen", "L", 0.001, 0.0 )
DEFU( 0x32, "Longitud", "pulgadas", 0.0000305, 0.0 )
DEFU( 0x33, "Ratio (lambda)", "", 0.00024414, 0.0 )
DEFU( 0x34, "Tiempo", "min", 1.0, 0.0 )
DEFU( 0x35, "Tiempo", "s", 0.01, 0.0 )
DEFU( 0x36, "Peso", "g", 0.01, 0.0 )
DEFU( 0x37, "Peso", "g", 0.1, 0.0 )
DEFU( 0x38, "Peso", "g", 1.0, 0.0 )
DEFU( 0x39, "Porcentaje", "%", 0.01, -32768.0 )

#endif /* MP_OBD2_UNIDADES_H */
