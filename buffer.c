/*
    Copyright 2021 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
   
    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */
/*
 * Control de Cambios
 *
 *
 * 27/12/2021   mapv    Escrito
 */


#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "obd2.h"

static unsigned char buffer[0x1000]; /* 0xFFF+1 */
static char hexbuffer[0xFFF*2+1]; 

static int len_buffer;

void set_buffer_len( int l )
{
	len_buffer = l;
}

int get_buffer_len( void )
{
	return len_buffer;
}

void set_buffer( const unsigned char * str )
{
	int i;

	for( i = 0; i < len_buffer; i++ )
		buffer[i] = *str++;
}
	

unsigned char * get_buffer( void )
{
	return buffer;
}

static unsigned char char2nibble( char c )
{
	if( c >= '0' && c <= '9' )
		return (c - '0');
	else if ( c >= 'A' && c <= 'F' )
		return ( 10 + ( c - 'A' ) );
	else if ( c >= 'a' && c <= 'f' )
		return ( 10 + ( c - 'a' ) );

	return 0xFF;
}

int show_buffer( void )
{
	int i;

	for( i = 0; i < len_buffer; i++ )
	{
		printf("%02X", buffer[i] );
	}
	printf("\n");

	return 0;
}

char * get_hexbuffer( void )
{
	return hexbuffer;
}

int set_hexbuffer( char * str )
{
	int hlen, i;
	char * ptr, * hptr;

	/* comprobamos que *str apunta a una cadena hexadecimal */

        ptr = str;

        while( *ptr != '\0' )
        {
                if( !isalnum( *ptr ) )
                {
                        return -1;
                }
                ptr++;
        }

	hptr = get_hexbuffer();

	/* La copiamos a hexbuffer */

	strcpy( hexbuffer, str );

	/* La convertimos para pasarla a buffer */

	hlen = strlen( hexbuffer );

	if( hlen % 2)
	{
		buffer[0] = char2nibble( *hptr  ); 
		i = 1;
		hptr++;
		hlen--;
	}
	else i = 0;

	while( i < hlen/2 )
	{
		buffer[i] = char2nibble( *hptr++ ) * 0x10;
		buffer[i] |= char2nibble( *hptr++ );
		i++;
	}

	obd2_set_serv( buffer[0] );
	obd2_set_pid(  buffer[1] );

	return 0;	
}

