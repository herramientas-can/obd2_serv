/*
    Copyright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
   
    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */

#ifndef MP_CAN_H
#define MP_CAN_H

#ifndef _CAN_H
#include <linux/can.h>
#endif

#define CAN_TIMEOUT	2
#define CAN_VERBOSE	0
#define ECU_BROADCAST	0x7DF
#define DEFAULT_PARAM	0

void can_set_timeout( unsigned int );
void can_set_verbose( unsigned int );
void set_ecu_address( unsigned int );
void set_default_param( unsigned int );

unsigned int can_get_timeout( void );
unsigned int can_get_verbose( void );
unsigned int get_ecu_address( void );
unsigned int get_default_param( void );

void can_timeout( int );
int can_abre_socket( void );
int can_cierra_socket( void );
int can_obd_filtro( void );
int can_referencia_interfaz( const char * );
int can_enlaza_interfaz( void );
int can_escribe_trama( struct can_frame * );
int can_lee_trama( struct can_frame * );

#endif /* MP_CAN_H */
