/*
    Copyright 2021 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)

    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */
/*
 * Control de Cambios
 *
 *
 * 27/12/2021   mapv    Escrito
 */

#ifndef MP_BUFFER_H
#define MP_BUFFER_H



void set_buffer_len( int l );
int get_buffer_len( void );

void set_buffer( const unsigned char * str );
int set_hexbuffer( char * str );

int show_buffer( void );

unsigned char * get_buffer( void );
char * get_hexbuffer( void );


#endif /* MP_BUFFER_H */

