/*
    Copyright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
   
    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */

#ifndef MP_OBD2_H
#define MP_OBD2_H

#define OBD_SERVICES	0xFF
#define OBD_PIDS	0xFF

#define OBD_FUEL	24
#define OBD_TYPES	35

#define OBD_UNIDADES	0xFF
#define OBD_S6_TEST_LEN	(9)

#define CAN_SF		(0x00)
#define CAN_FF		(0x10)
#define CAN_CF		(0x20)
#define CAN_FC		(0x30)

#define CAN_FC_CONTINUE	0x0
#define CAN_FC_WAIT	0x1
#define CAN_FC_OVERFLOW	0x2

#define NO_EXT_ADDRESS	(-1)

#define LOCAL		static
#define PUBLIC		

#define DTC_U		0
#define DTC_P		1
#define	DTC_C		2
#define DTC_B		3

#define OBD2_DTC_TYPES	4
#define OBD2_MAX_DTCS	0x4000

struct obd2_dtc	{
	int i;
	char * desc;
} obd2_dtc_desc[OBD2_DTC_TYPES][OBD2_MAX_DTCS];

#define DTC(T,I,D)	\
	obd2_dtc_desc[T][I].desc = D;

struct obd2_aux {
	int soportado;					/* Flag */
	int len;					/* longitud de los datos - excluendo servicio y PID */
	char * desc;					/* Descripción del parámetro*/
	char * unidades;				/* Descripción de la unidades*/
	int (*manejador_obd2)( unsigned char *, int );
} obd2_data[OBD_SERVICES][OBD_PIDS];

#define FDEF(S,P,L,D,U,F) 	\
	obd2_data[S][P].len = L; \
	obd2_data[S][P].desc = D; \
	obd2_data[S][P].unidades = U; \
	obd2_data[S][P].manejador_obd2 = F;

#define DEF(S,P,L,D) 	\
	obd2_data[S][P].len = L; \
	obd2_data[S][P].desc = D;

struct obd2_unidades {
	char * desc;					/* Descripción del parámetro*/
	char * unidades;
	float escala;
	float offset;
} obd2_unidades[OBD_UNIDADES];

#define DEFU( I, D, U, E, O ) \
	obd2_unidades[I].desc = D; \
	obd2_unidades[I].unidades = U; \
	obd2_unidades[I].escala = E; \
	obd2_unidades[I].offset = O; \

void set_ext_address( int );
int get_ext_address( void );

void obd2_inicializa_funciones( void );
int obd2_scan( int );
int obd2_start( int );
int obd2_pid( int, int);

void obd2_set_dato( unsigned int, unsigned int );
void obd2_unset_dato( unsigned int );
int obd2_send( int );

void obd2_set_long( unsigned int );
void obd2_set_serv( unsigned int );
void obd2_set_pid( unsigned int );

unsigned int obd2_get_long ( void );
unsigned int obd2_get_serv ( void );
unsigned int obd2_get_pid ( void );

char * obd2_get_dato( int );

int obd2_send_buffer( unsigned int, unsigned char *, int );

#endif /* MP_OBD2_H */
