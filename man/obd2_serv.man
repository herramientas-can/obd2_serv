.\"
.\"    Copyright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
.\"
.\"    This file is part of obd2_serv
.\"
.\"    obd2_serv is free software: you can redistribute it and/or modify
.\"    it under the terms of the GNU General Public License as published by
.\"    the Free Software Foundation, either version 3 of the License, or
.\"    (at your option) any later version.
.\"
.\"    obd2_serv is distributed in the hope that it will be useful,
.\"    but WITHOUT ANY WARRANTY; without even the implied warranty of
.\"    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\"    GNU General Public License for more details.
.\"
.\"    You should have received a copy of the GNU General Public License
.\"    along with obd2_serv  If not, see <https://www.gnu.org/licenses/>.
.\"
.TH obd2_serv 1 "27 Nov 2019" "1.0" "Página de manual de obd2_serv"
.
.SH NOMBRE
obd2_serv \- explora los servicios obdii de la centralitas conectadas al bus CAN en uso.
.
.SH SINOPSIS
.B obd2_serv
[OPCIONES] INTERFAZ
.SH DESCRIPCION
obd2_serv permite explorar los distintos servicios OBDII que soportan las centralitas conectadas al INTERFAZ especificado (usualmente can0), explorando que identificadores soportan (si aplica), interrogando a la centralita por cada uno de ellos, e interpretando la respuesta recibida según el protocolo ISO 15031-5 (SAE J1979).
.SH OPCIONES
obd2_serv soporta las siguientes opciones:
.TP
.BR "-s SERVICIO"
Especifica el servicio OBDII (en decimal) a utilizar. Por defecto se usa 1.
.TP
.BR "-t TIMEOUT"
Especifica el temporizador de protocolo a utilizar, en segundos. Por defecto se usa 2 segundos
.TP
.BR "-v TRAZA"
Especifica el nivel de trazas (por defecto 0)
.RS
.TP
.B 0
no muestra trazas
.TP
.B 1
muestra trazas de protocolo
.TP
.B > 1
muestra información extendida (normalmente útil unicamente a efectos de desarrollo)
.RE
.TP
.BR "-h"
Muestra la ayuda.
.SH EJEMPLOS
.TP
.BR "obd2_serv -s 6 can1"
Explora por el interfaz can1 el servicio 6 de obdii.
.SH VER TAMBIÉN
odbii(1)
.SH BUGS
Dado que el programa se ha desarrollado realizando pruebas sobre un Toyota GT86, únicamente se soportan los servicios que soporta dicho vehiculo, y para esos servicios, únicamente los PIDs que soporta ese modelo. El programa se ha diseñado para extender de forma sencilla la funcionalidad a otros servicios y PIDs.
.SH AUTOR
Miguel Angel Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)

