/*
    Copyright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
   
    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <endian.h>
#include <signal.h>
#include <errno.h>
#include <limits.h>

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "can.h"
#include "obd2.h"
#include "lenguaje.h"

/*
 * Uso del programa
 */
void usage( void )
{
	/*
	 * Por definir ...
	 */
	puts("");
	puts("Use:");
	puts("	obd2_modes [-h][-s <servicio>][-t <timeout>][-e <ecu>][-p <param>][-v <traza>] interfaz");
	puts("");
	puts("Opciones:");
	puts("  -h		Muestra esta ayuda");
	puts("  -i <fichero>	Especifica el fichero de comandos a ejecutar");
	puts("  -s <servicio>	Especifica el servicio OBD en hexadecimal (por defecto 0x01)");
	printf("  -e <ecu>	Especifica la dirección de la ECU a interrogar en hexadecimal (por defecto 0x%X)\n", ECU_BROADCAST );
	printf("  -p <param>	Especifica un parámetro a pasar al servicio, en hexadecimal (por defecto 0x%X)\n", DEFAULT_PARAM );
	printf("  -t <timeout>	Especifica el timeout del protocolo en segundos (por defecto %d s.)\n", CAN_TIMEOUT );
	printf("  -x <ext>	Especifica la dirección extendida de la ECU en hexadecimal\n" );
	printf("  -f <frec>	Especifica la frecuencia de repetición de trama (por defecto %d)\n", 0 );
	printf("  -v <traza>	Especifica el nivel de traza (por defecto %d)\n", CAN_VERBOSE );
	puts("  			0  - no muestra trazas");
	puts("  			1  - muestra tramas de protocolo");
	puts("  			>1 - muestra información adicional ");
	puts("");
	puts("Obligatorio:");
	puts("  interfaz:		Especifica el interfaz can a usar");
	puts("");
	puts("Ejemplo:");
	puts("	obd2_modes -s 9 can0");
	puts("");
	puts("");
}


/*
 * Función principal
 */
int main(int argc, char** argv) {

    /*
     * Options
     */
    const char* interface;

    /*
     */
    int rc;
    char * endptr;

    char _servicio_obd[16];
    int servicio_obd = 0x01;
	
    char _can_vtimeout[16];
    int can_vtimeout = CAN_TIMEOUT;
	
    char _can_verbose[16];
    int can_verbose = CAN_VERBOSE;
	
    char _ecu_address[16];
    int ecu_address = ECU_BROADCAST;
	
    char _def_param[16];
    int def_param = DEFAULT_PARAM;
	
    char _frecuencia[16];
    int frecuencia = 0;
	
    char _ext_address[16];
    int ext_address = NO_EXT_ADDRESS;
	
    char fichero_entrada[64];
    int iflag = 0;
	
    /*
     *  Parse command line arguments
     */
        int opt;

        /*
	 *  Parse option flags
	 */
        while ((opt = getopt(argc, argv, "hs:t:v:e:p:f:x:i:")) != -1)
	{
            switch (opt)
	    {
            	case 'h':
                	usage();
                	return EXIT_SUCCESS;

		case 'i':
                        strcpy( fichero_entrada, optarg );
			iflag = 1;

			break;

		case 'e':
                        strcpy( _ecu_address, optarg );

                        ecu_address = strtol( _ecu_address, &endptr, 16 ); /* Base 16 */

                        /* Validación de posibles errores */

                        if (endptr == _ecu_address)
                        {
                                fprintf(stderr, "No se encontró valor para la dirección de la ECU\n");
                                exit(EXIT_FAILURE);
                        }

                        break;

		case 'f':
                        strcpy( _frecuencia, optarg );

                        frecuencia = strtol( _frecuencia, &endptr, 10 );

                        /* Validación de posibles errores */

                        if (endptr == _frecuencia)
                        {
                                fprintf(stderr, "No se encontró valor para la frecuencia de muestreo\n");
                                exit(EXIT_FAILURE);
                        }

                        break;

		case 'p':
                        strcpy( _def_param, optarg );

                        def_param = strtol( _def_param, &endptr, 16 ); /* Base 16 */

                        /* Validación de posibles errores */

                        if (endptr == _def_param)
                        {
                                fprintf(stderr, "No se encontró valor para el parámetro del servicio\n");
                                exit(EXIT_FAILURE);
                        }

                        break;

		case 's':
                        strcpy( _servicio_obd, optarg );

                        servicio_obd = strtol( _servicio_obd, &endptr, 16 ); /* Base 16 */

                        /* Validación de posibles errores */

                        if (endptr == _servicio_obd)
                        {
                                fprintf(stderr, "No se encontró valor para el servicio OBD2\n");
                                exit(EXIT_FAILURE);
                        }

                        break;

		case 't':
                        strcpy( _can_vtimeout, optarg );

                        can_vtimeout = strtol( _can_vtimeout, &endptr, 10 ); /* Base 10 */

                        /* Validación de posibles errores */

                        if (endptr == _can_vtimeout)
                        {
                                fprintf(stderr, "No se encontró valor para el timeout del protocolo CAN\n");
                                exit(EXIT_FAILURE);
                        }

                        break;

		case 'v':
                        strcpy( _can_verbose, optarg );

                        can_verbose = strtol( _can_verbose, &endptr, 10 ); /* Base 10 */

                        /* Validación de posibles errores */

                        if (endptr == _can_verbose)
                        {
                                fprintf(stderr, "No se encontró valor para el nivel de trazas\n");
                                exit(EXIT_FAILURE);
                        }

                        break;

		case 'x':
                        strcpy( _ext_address, optarg );

                        ext_address = strtol( _ext_address, &endptr, 16 ); /* Base 16 */

                        /* Validación de posibles errores */

                        if (endptr == _ext_address)
                        {
                                fprintf(stderr, "No se encontró valor para el nivel de trazas\n");
                                exit(EXIT_FAILURE);
                        }

			if( ext_address > 0xFF )
			{
				
                                fprintf(stderr, "La extensión de la dirección no puede ser máyor de 0xFF\n");
                                exit(EXIT_FAILURE);
			}
                        break;

            	default:
                	usage();
                	return EXIT_FAILURE;
           }
        }

        /*
	 * Check for the one positional argument
	 */
        if (optind != (argc - 1)) {
            puts("");
            puts("Es obligatorio especificar un interfaz can!!!");
            puts("");
            usage();
            return EXIT_FAILURE;
        }

        /*
	 * Set the network interface to use
	 */
        interface = argv[optind];

	/*
	 * Inicializamos la matriz de funciones manejadoras del OBD
	 */
	obd2_inicializa_funciones();

        /*
         * Registramos los manejadores de señales
         *
        if( SIG_ERR == signal(SIGINT, onsig) )
        {
                perror("Error gestionando manejador para SIGINT");
                return errno;
        }

        if( SIG_ERR == signal(SIGTERM, onsig) )
        {
                perror("Error gestionando manejador para SIGTERM");
                return errno;
        }

        if( SIG_ERR == signal(SIGQUIT, onsig) )
        {
                perror("Error gestionando manejador para SIGTERM");
                return errno;
        }

        if( SIG_ERR == signal(SIGHUP, onsig) )
        {
                perror("Error gestionando manejador para SIGTERM");
                return errno;
        }

        if( SIG_ERR == signal(SIGCHLD, SIG_IGN) )
        {
                perror("Error gestionando manejador para SIGCHILD");
                return errno;
        }
	*/

	can_set_timeout( can_vtimeout );
	can_set_verbose( can_verbose );
	set_ecu_address( ecu_address );
	set_ext_address( ext_address );
	set_default_param( def_param );

        if( SIG_ERR == signal(SIGALRM, can_timeout) )
        {
                perror("Error gestionando manejador para SIGALARM");
                return errno;
        }

        /*
         *  Inicializamos a 0 el valor de la señal
        sigval = 0;
         */

	/*
	 * Abrimos el socket para conectarnos al bus CAN
	 */
	rc = can_abre_socket();

        if (rc < 0)
        {
                return -1;
        }

	/*
	 * Fijamos los filtros para solo recibir los PIDS mayores de 0x7DF
	 */
	rc = can_obd_filtro();

        if (rc < 0)
        {
		rc = can_cierra_socket();
                return -1;
        }

        /*
         *  Obtenemos la referencia al interfaz de red 
         */
	rc = can_referencia_interfaz( interface );

        if( rc < 0)
        {
		rc = can_cierra_socket();
                return -2;
        }

        /*
         * Enlazamos el socket con el interfaz
         */
	rc = can_enlaza_interfaz();

        if( rc < 0)
        {
		rc = can_cierra_socket();
                return -3;
        }


	if( iflag == 1 ) /* procesamos el fichero */
        	rc = interprete( fichero_entrada );
	else if ( frecuencia == 0 )
		rc = obd2_start( servicio_obd );
	else
		rc = obd2_pid( servicio_obd, frecuencia );

        return rc;
}
