/*
    Copyright 2021 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
   
    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */

#ifndef MP_ORDENES_H
#define MP_ORDENES_H

#define MAX_ORDERS	48

struct obd2_syn {
	char * orden;
	int  tipo;
	int (*manejador_sintactico)( int );
	char * desc;
} obd2_syntax[MAX_ORDERS];

#define SYN(I,T,O,M,D)	\
	obd2_syntax[I].orden = O; \
	obd2_syntax[I].tipo = T; \
	obd2_syntax[I].manejador_sintactico = M; \
	obd2_syntax[I].desc = D;

int syn_setecu( int );
int syn_setlen( int );
int syn_setser( int );
int syn_setpid( int );
int syn_settmo( int );
int syn_settrc( int );
int syn_setd0( int );
int syn_setd1( int );
int syn_setd2( int );
int syn_setd3( int );
int syn_setd4( int );
int syn_setd5( int );
int syn_setd6( int );
int syn_setd7( int );
int syn_setext( int );
int syn_send( int );
int syn_help( int );
int syn_exit( int );
int syn_showecu( int );
int syn_showlen( int );
int syn_showser( int );
int syn_showpid( int );
int syn_showtmo( int );
int syn_showtrc( int );
int syn_showd0( int );
int syn_showd1( int );
int syn_showd2( int );
int syn_showd3( int );
int syn_showd4( int );
int syn_showd5( int );
int syn_showd6( int );
int syn_showd7( int );
int syn_showext( int );
int syn_showdtc( int );
int syn_chkpid( int );
int syn_execute( int );
int syn_status( int );
int syn_scanser( int );
int syn_setbuffer( int );
int syn_showbuffer( int );
int syn_sendbuffer( int );
int syn_showlenbuffer( int );

#endif /*  MP_ORDENES_H */
