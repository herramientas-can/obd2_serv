/*
    Copyright 2021 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)

    This file is part of obd2_serv.

    obd2_serv is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    obd2_serv is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with obd2_serv.  If not, see <https://www.gnu.org/licenses/>.

 */

/*
 * 13/09/2021   mapv    Escrito
 */

#ifndef MP_CDTC_H
#define MP_CDTC_H

#ifndef MP_OBD2_H
include "obd2.h"
#endif  /* MP_OBD2_H */

DTC( DTC_C, 0x0, "Vehicle Speed Information Circuit Malfunction" )
DTC( DTC_C, 0x35, "Left Front Wheel Speed Circuit Malfunction" )
DTC( DTC_C, 0x40, "Right Front Wheel Speed Circuit Malfunction" )
DTC( DTC_C, 0x41, "Right Front Wheel Speed Sensor Circuit Range/Performance (EBCM)" )
DTC( DTC_C, 0x45, "Left Rear Wheel Speed Circuit Malfunction" )
DTC( DTC_C, 0x46, "Left Rear Wheel Speed Sensor Circuit Range/Performance (EBCM)" )
DTC( DTC_C, 0x50, "Right Rear Wheel Speed Circuit Malfunction" )
DTC( DTC_C, 0x51, "LF Wheel Speed Sensor Circuit Range/Performance (EBCM)" )
DTC( DTC_C, 0x60, "Left Front ABS Solenoid #1 Circuit Malfunction" )
DTC( DTC_C, 0x65, "Left Front ABS Solenoid #2 Circuit Malfunction" )
DTC( DTC_C, 0x70, "Right Front ABS Solenoid #1 Circuit Malfunction" )
DTC( DTC_C, 0x75, "Right Front ABS Solenoid #2 Circuit Malfunction" )
DTC( DTC_C, 0x80, "Left Rear ABS Solenoid #1 Circuit Malfunction" )
DTC( DTC_C, 0x85, "Left Rear ABS Solenoid #2 Circuit Malfunction" )
DTC( DTC_C, 0x90, "Right Rear ABS Solenoid #1 Circuit Malfunction" )
DTC( DTC_C, 0x95, "Right Rear ABS Solenoid #2 Circuit Malfunction" )
DTC( DTC_C, 0x110, "Pump Motor Circuit Malfunction" )
DTC( DTC_C, 0x121, "Valve Relay Circuit Malfunction" )
DTC( DTC_C, 0x128, "Low Brake Fluid Circuit Low" )
DTC( DTC_C, 0x141, "Left TCS Solenoid #1 Circuit Malfunction" )
DTC( DTC_C, 0x146, "Left TCS Solenoid #2 Circuit Malfunction" )
DTC( DTC_C, 0x151, "Right TCS Solenoid #1 Circuit Malfunction" )
DTC( DTC_C, 0x156, "Right TCS Solenoid #2 Circuit Malfunction" )
DTC( DTC_C, 0x161, "ABS/TCS Brake Switch Circuit Malfunction" )
DTC( DTC_C, 0x221, "Right Front Wheel Speed Sensor Circuit Open" )
DTC( DTC_C, 0x222, "Right Front Wheel Speed Signal Missing" )
DTC( DTC_C, 0x223, "Right Front Wheel Speed Signal Erratic" )
DTC( DTC_C, 0x225, "Left Front Wheel Speed Sensor Circuit Open" )
DTC( DTC_C, 0x226, "Left Front Wheel Speed Signal Missing" )
DTC( DTC_C, 0x227, "Left Front Wheel Speed Signal Erratic" )
DTC( DTC_C, 0x229, "Drop Out of Front Wheel Speed Signals" )
DTC( DTC_C, 0x235, "Rear Wheel Speed Signal Circuit Open" )
DTC( DTC_C, 0x236, "Rear Wheel Speed Signal Circuit Missing" )
DTC( DTC_C, 0x237, "Rear Wheel Speed Signal Erratic" )
DTC( DTC_C, 0x238, "Wheel Speed Mismatch" )
DTC( DTC_C, 0x241, "EBCM Control Valve Circuit" )
DTC( DTC_C, 0x245, "Wheel Speed Sensor Frequency Error" )
DTC( DTC_C, 0x254, "EBCM Control Valve Circuit" )
DTC( DTC_C, 0x265, "EBCM Relay Circuit" )
DTC( DTC_C, 0x266, "EBCM Relay Circuit" )
DTC( DTC_C, 0x267, "Pump Motor Circuit Open/Shorted" )
DTC( DTC_C, 0x268, "Pump Motor Circuit Open/Shorted" )
DTC( DTC_C, 0x269, "Excessive Dump/Isolation Time" )
DTC( DTC_C, 0x271, "EBCM Malfunction" )
DTC( DTC_C, 0x272, "EBCM Malfunction" )
DTC( DTC_C, 0x273, "EBCM Malfunction" )
DTC( DTC_C, 0x274, "Excessive Dump/Isolation Time" )
DTC( DTC_C, 0x279, "Powertrain Configuration Not Valid" )
DTC( DTC_C, 0x281, "Brake Switch Circuit" )
DTC( DTC_C, 0x283, "Traction Switch Shorted to Ground" )
DTC( DTC_C, 0x284, "EBCM Malfunction" )
DTC( DTC_C, 0x286, "ABS Indicator Lamp Circuit Shorted to B+" )
DTC( DTC_C, 0x287, "Delivered Torque Circuit" )
DTC( DTC_C, 0x288, "Brake Warning Lamp Circuit Shorted to B+" )
DTC( DTC_C, 0x290, "Lost Communications With PCM" )
DTC( DTC_C, 0x291, "Lost Communications With BCM" )
DTC( DTC_C, 0x292, "Lost Communications With PCM" )
DTC( DTC_C, 0x297, "Powertrain Configuration Data Not Received" )
DTC( DTC_C, 0x298, "Powertrain Indicated Traction Control Malfunction" )
DTC( DTC_C, 0x300, "Rear Speed Sensor Malfunction" )
DTC( DTC_C, 0x305, "Front Speed Sensor Malfunction" )
DTC( DTC_C, 0x306, "Motor A or B Circuit" )
DTC( DTC_C, 0x308, "Motor A/B Circuit Low" )
DTC( DTC_C, 0x309, "Motor A/B Circuit High" )
DTC( DTC_C, 0x310, "Motor A/B Circuit Open" )
DTC( DTC_C, 0x315, "Motor Ground Circuit Open" )
DTC( DTC_C, 0x321, "Transfer Case Lock Circuit" )
DTC( DTC_C, 0x323, "T-Case Lock Circuit Low" )
DTC( DTC_C, 0x324, "T-Case Lock Circuit High" )
DTC( DTC_C, 0x327, "Encoder Circuit Malfunction" )
DTC( DTC_C, 0x357, "Park Switch Circuit High" )
DTC( DTC_C, 0x359, "Four Wheel Drive Low Range (4LO) Discrete Output Circuit" )
DTC( DTC_C, 0x362, "4LO Discrete Output Circuit High" )
DTC( DTC_C, 0x367, "Front Axle Control Circuit High" )
DTC( DTC_C, 0x374, "General System Malfunction" )
DTC( DTC_C, 0x376, "Front/Rear Shaft Speed Mismatch" )
DTC( DTC_C, 0x379, "Front Axle System" )
DTC( DTC_C, 0x387, "Unable to Perform Shift" )
DTC( DTC_C, 0x472, "Steering Handwheel Speed Sensor Signal V Low" )
DTC( DTC_C, 0x473, "Steering Handwheel Speed Sensor Signal V High" )
DTC( DTC_C, 0x495, "EVO Tracking Error" )
DTC( DTC_C, 0x498, "Steering Assist Control Actuator Feed Circuit Low" )
DTC( DTC_C, 0x499, "Steering Assist Control Solenoid Feed Circuit High" )
DTC( DTC_C, 0x503, "Steering Assist Control Solenoid Return Circuit Low" )
DTC( DTC_C, 0x504, "Steering Assist Control Solenoid Return Circuit High" )
DTC( DTC_C, 0x550, "ECU Malfunction - internal write / checksum malfunction" )
DTC( DTC_C, 0x559, "EEPROM Checksum Error" )
DTC( DTC_C, 0x563, "Calibration ROM Checksum Error" )
DTC( DTC_C, 0x577, "Left Front Solenoid Circuit Low" )
DTC( DTC_C, 0x578, "Left Front Solenoid Circuit High" )
DTC( DTC_C, 0x579, "Left Front Solenoid Circuit Open" )
DTC( DTC_C, 0x582, "Right Front Solenoid Circuit Low" )
DTC( DTC_C, 0x583, "Right Front Solenoid Circuit High" )
DTC( DTC_C, 0x584, "Right Front Solenoid Circuit Open" )
DTC( DTC_C, 0x587, "Left Rear Solenoid Circuit Low" )
DTC( DTC_C, 0x588, "Left Rear Solenoid Circuit High" )
DTC( DTC_C, 0x589, "Left Rear Solenoid Circuit Open" )
DTC( DTC_C, 0x592, "Right Rear Solenoid Circuit Low" )
DTC( DTC_C, 0x593, "Right Rear Solenoid Circuit High" )
DTC( DTC_C, 0x594, "Right Rear Solenoid Circuit Open" )
DTC( DTC_C, 0x611, "VIN Information Error" )
DTC( DTC_C, 0x615, "Left Front Position Sensor Malfunction" )
DTC( DTC_C, 0x620, "Right Front Position Sensor Malfunction" )
DTC( DTC_C, 0x625, "Left Rear Position Sensor Malfunction" )
DTC( DTC_C, 0x628, "Level Control Position Sensor Circuit High" )
DTC( DTC_C, 0x630, "Right Rear Position Sensor Malfunction" )
DTC( DTC_C, 0x635, "Left Front Normal Force Circuit Malfunction" )
DTC( DTC_C, 0x638, "Left Front Normal Force Circuit High" )
DTC( DTC_C, 0x640, "Right Front Normal Force Circuit Malfunction" )
DTC( DTC_C, 0x643, "Right Front Normal Force Circuit High" )
DTC( DTC_C, 0x655, "Level Control Compressor Relay Malfunction" )
DTC( DTC_C, 0x657, "Level Control Compressor Circuit Low" )
DTC( DTC_C, 0x658, "Level Control Compressor Circuit High" )
DTC( DTC_C, 0x660, "Level Control Exhaust Valve Circuit Malfunction" )
DTC( DTC_C, 0x662, "Level Control Exhaust Valve Circuit Low" )
DTC( DTC_C, 0x663, "Level Control Exhaust Valve Circuit High" )
DTC( DTC_C, 0x665, "Chassis Pitch Signal Circuit" )
DTC( DTC_C, 0x690, "Damper Control Relay Circuit Malfunction" )
DTC( DTC_C, 0x691, "Damper Control Relay Circuit Range" )
DTC( DTC_C, 0x693, "Damper Control Relay Circuit High" )
DTC( DTC_C, 0x695, "Position Sensor Overcurrent (8 volt supply)" )
DTC( DTC_C, 0x696, "Position Sensor Overcurrent (5 volt supply)" )
DTC( DTC_C, 0x710, "Steering Position Signal Malfunction" )
DTC( DTC_C, 0x750, "Tire Pressure Monitor (TPM) system sensor not transmitting" )
DTC( DTC_C, 0x755, "Tire Pressure Monitor (TPM) system sensor not transmitting" )
DTC( DTC_C, 0x760, "Tire Pressure Monitor (TPM) system sensor not transmitting" )
DTC( DTC_C, 0x765, "Tire Pressure Monitor (TPM) system sensor not transmitting" )
DTC( DTC_C, 0x800, "Device Power #1 Circuit Malfunction" )
DTC( DTC_C, 0x896, "Electronic Suspension Control (ESC) voltage is outside the normal range of 9 to 15.5 volts" )

#endif  /* MP_CDTC_H */
